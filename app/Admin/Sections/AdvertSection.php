<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;

use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;
use Meta;
use AdminColumnEditable;

use App\ObjType;
use App\RentType;
use App\District;
use App\User;
use App\Option;
use App\Feature;
use App\DeclineType;

/**
 * Class Advert
 *
 * @property \Advert $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class AdvertSection extends Section implements Initializable
{
    /**
     * Model
     * @var string
     */
    protected $model = '\App\Advert';

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Объявления';

    /**
     * @var string
     */
    protected $alias = 'adverts';

    /**
     * Initialize section
     */
    public function initialize()
    {
        $this->addToNavigation()->setIcon('fa fa-list')->setPriority(10);
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setOrder([[1, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('title', 'Заголовок'),
                AdminColumn::text('objtype.name', 'Вид недвижимости'),
                AdminColumn::text('renttype.name', 'Тип аренды'),
                AdminColumn::text('price', 'Цена'),
                AdminColumn::text('created_at', 'Дата создания'),
                AdminColumnEditable::checkbox('publicated', 'Да', 'Нет')->setLabel('Публикация')
            )->paginate(25);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        Meta::addJs('admin-adverts-yandex', 'https://api-maps.yandex.ru/2.1/?lang=ru_RU', 'admin-default');
        Meta::addJs('admin-adverts-map', asset('packages/sleepingowl/custom/adverts_map.js'), 'admin-default');
        return AdminForm::panel()->addBody([
            AdminFormElement::select('obj_type_id', 'Вид недвижимости', ObjType::class)->setDisplay('name')->required(),
            AdminFormElement::text('title', 'Заголовок')->required()->addValidationRule('string'),
            AdminFormElement::text('price', 'Цена')->required()->addValidationRule('integer'),
            AdminFormElement::select('district_id', 'Район', District::class)->setDisplay('name')->required(),
            AdminFormElement::text('address', 'Адрес')->required()->addValidationRule('string'),
            AdminFormElement::view('admin.adverts.map'),
            AdminFormElement::select('rent_type_id', 'Тип аренды', RentType::class)->setDisplay('name')->required(),
            AdminFormElement::number('room_num', 'Кол-во комнат')->setMax(10)->setMin(1)->setStep(1),
            AdminFormElement::text('square', 'Площадь')->required()->addValidationRule('integer'),
            AdminFormElement::text('floor', 'Этаж')->required()->addValidationRule('integer'),
            AdminFormElement::multiselect('features', 'Особенности', Feature::class)->setDisplay('name'),
            AdminFormElement::multiselect('options', 'Удобства', Option::class)->setDisplay('name'),
            AdminFormElement::wysiwyg('text', 'Описание')->required()->addValidationRule('string'),
            AdminFormElement::images('images', 'Изображения')->required(),
            AdminFormElement::selectajax('user_id', 'Пользователь')->setModelForOptions(User::class)->setDisplay('email')->required(),
            AdminFormElement::text('phone_num', 'Номер телефона')->required(),
            AdminFormElement::multiselect('declineTypes', 'Отклонить', DeclineType::class)->setDisplay('name'),
            AdminFormElement::checkbox('publicated', 'Публикация'),
            AdminFormElement::hidden('yandex_coord')
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
