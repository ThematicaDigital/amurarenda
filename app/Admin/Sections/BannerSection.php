<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;

use AdminDisplay;
use AdminColumn;
use AdminColumnEditable;
use AdminForm;
use AdminFormElement;

/**
 * Class Banner
 *
 * @property \App/Banner $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class BannerSection extends Section implements Initializable
{
    /**
     * Model
     * @var string
     */
    protected $model = '\App\Banner';

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Баннера';

    /**
     * @var string
     */
    protected $alias = 'banners';

    /**
     * Initialize section
     */
    public function initialize()
    {
        $this->addToNavigation()->setIcon('fa fa-image')->setPriority(10);
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setOrder([[1, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('title', 'Заголовок'),
                AdminColumn::image('img', 'Изображение'),
                AdminColumn::url('link', 'Ссылка'),
                AdminColumn::text('slug', 'Алиас'),
                AdminColumn::text('created_at', 'Дата создания'),
                AdminColumnEditable::checkbox('publicated', 'Да', 'Нет')->setLabel('Публикация')
            )->paginate(25);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('title', 'Заголовок')->required()->addValidationRule('string'),
            AdminFormElement::text('link', 'Ссылка')->required()->addValidationRule('string'),
            AdminFormElement::image('img', 'Изображения')->required(),
            AdminFormElement::checkbox('publicated', 'Публикация'),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
