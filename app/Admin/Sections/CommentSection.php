<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;

use AdminDisplay;
use AdminColumn;
use AdminColumnEditable;
use AdminForm;
use AdminFormElement;

/**
 * Class CommentSection
 *
 * @property \App/Comment $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class CommentSection extends Section implements Initializable
{
    /**
     * Model
     * @var string
     */
    protected $model = '\App\Comment';

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Отзывы';

    /**
     * @var string
     */
    protected $alias = 'comments';

    /**
     * Initialize section
     */
    public function initialize()
    {
        $this->addToNavigation()->setIcon('fa fa-comment-o')->setPriority(10);
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setOrder([[1, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('name', 'Имя'),
                AdminColumn::relatedLink('author.email', 'Пользователь'),
                AdminColumn::relatedLink('advert.title', 'Объявление'),
                AdminColumn::text('text', 'Отзыв'),
                AdminColumn::text('created_at', 'Дата создания'),
                AdminColumnEditable::checkbox('publicated', 'Да', 'Нет')->setLabel('Публикация')
            )->paginate(25);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('name', 'Имя')->required()->addValidationRule('string'),
            AdminFormElement::textarea('text', 'Отзыв')->required()->addValidationRule('string'),
            AdminFormElement::checkbox('publicated', 'Публикация'),
        ]);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
