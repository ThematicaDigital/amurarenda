<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;

use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;

/**
 * Class PaymentSection
 *
 * @property \App/Payment $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class PaymentSection extends Section implements Initializable
{
    /**
     * Model
     * @var string
     */
    protected $model = '\App\Payment';

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Платежи';

    /**
     * @var string
     */
    protected $alias = 'payments';

    /**
     * Initialize section
     */
    public function initialize()
    {
        $this->addToNavigation()->setIcon('fa fa-money')->setPriority(10);
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setOrder([[1, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('sum', 'Сумма'),
                AdminColumn::relatedLink('client.email', 'Клиент'),
                AdminColumn::relatedLink('tariff.title', 'Тарифный план'),
                AdminColumn::text('status_title', 'Статус'),
                AdminColumn::text('created_at', 'Дата создания'),
                AdminColumn::text('updated_at', 'Дата обновления')
            )->paginate(25);
    }

    /**
     * Check is deletable
     *
     * @param \Illuminate\Database\Eloquent\Model $model
     * @return boolean
     */
    public function isDeletable(\Illuminate\Database\Eloquent\Model $model)
    {
        return false;
    }
}
