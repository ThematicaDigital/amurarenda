<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;

use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;

/**
 * Class Request
 *
 * @property \App/Request $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class RequestSection extends Section implements Initializable
{
    /**
     * Model
     * @var string
     */
    protected $model = '\App\Request';

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Заявки';

    /**
     * @var string
     */
    protected $alias = 'requests';

    /**
     * Initialize section
     */
    public function initialize()
    {
        $this->addToNavigation()->setIcon('fa fa-bell')->setPriority(10);
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setOrder([[1, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('name', 'Имя'),
                AdminColumn::text('phone', 'Телефон'),
                AdminColumn::email('email', 'E-mail'),
                AdminColumn::text('text', 'Сообщение'),
                AdminColumn::text('created_at', 'Дата создания')
            )->paginate(25);
    }
}
