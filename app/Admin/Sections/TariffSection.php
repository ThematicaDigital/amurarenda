<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;

use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;

/**
 * Class Tariff
 *
 * @property \App/Tariff $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class TariffSection extends Section implements Initializable
{
    /**
     * Model
     * @var string
     */
    protected $model = '\App\Tariff';

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Тарифные планы';

    /**
     * @var string
     */
    protected $alias = 'tariffs';

    /**
     * Initialize section
     */
    public function initialize()
    {
        $this->addToNavigation()->setIcon('fa fa-money')->setPriority(10);
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setOrder([[1, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::text('title', 'Название'),
                AdminColumn::text('price', 'Цена'),
                AdminColumn::text('created_at', 'Дата создания')
            )->paginate(25);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('title', 'Название')->required()->addValidationRule('string'),
            AdminFormElement::wysiwyg('description', 'Описание')->required(),
            AdminFormElement::number('price', 'Цена'),
            AdminFormElement::number('price_old', 'Старая цена'),
            AdminFormElement::number('days', 'Кол-во дней активности'),
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
