<?php

namespace App\Admin\Sections;

use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;

use AdminDisplay;
use AdminColumn;
use AdminForm;
use AdminFormElement;

/**
 * Class User
 *
 * @property \User $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class UserSection extends Section implements Initializable
{
    /**
     * Model
     * @var string
     */
    protected $model = '\App\User';

    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Пользователи';

    /**
     * @var string
     */
    protected $alias = 'users';

    /**
     * Initialize section
     */
    public function initialize()
    {
        $this->addToNavigation()->setIcon('fa fa-users')->setPriority(10);
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::datatables()
            ->setOrder([[1, 'desc']])
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                AdminColumn::text('id', '#')->setWidth('30px'),
                AdminColumn::email('email', 'E-mail'),
                AdminColumn::relatedLink('tariff.title', 'Тарифный план'),
                AdminColumn::datetime('tariff_start', 'Дата активации тарифа')->setFormat('Y-m-d'),
                AdminColumn::relatedLink('role.title', 'Группа доступа'),
                AdminColumn::text('created_at', 'Дата регистрации')
            )->paginate(25);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::select('role_id', 'Группа доступа', \App\Role::class, 'title')->required(),
            AdminFormElement::text('email', 'E-mail')->required()->addValidationRule('string'),
            !empty($id) ?
            AdminFormElement::password('password', 'Пароль')->hashWithBcrypt() :
            AdminFormElement::password('password', 'Пароль')->hashWithBcrypt()->required()->addValidationRule('string'),
            AdminFormElement::select('tariff_id', 'Тарифный план', \App\Tariff::class, 'title')->nullable(),
            AdminFormElement::date('tariff_start', 'Дата активации тарифного плана')->setPickerFormat('Y-m-d')->setCurrentDate()
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
