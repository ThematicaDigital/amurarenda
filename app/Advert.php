<?php namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Folklore\Image\Facades\Image;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Advert
 *
 * @property int $id
 * @property string $title
 * @property string $text
 * @property string $images
 * @property int $room_num
 * @property int $price
 * @property int $obj_type_id
 * @property int $rent_type_id
 * @property int $square
 * @property string $phone_num
 * @property string $yandex_coord
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $slug
 * @property bool $publicated
 * @property int $user_id
 * @property string $title_img
 * @property string $address
 * @property int $district_id
 * @property int $floor
 * @property-read \App\User $author
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DeclineType[] $declinetypes
 * @property-read \App\District $district
 * @property \Illuminate\Database\Eloquent\Collection|\App\Feature[] $features
 * @property-read \App\ObjType $objtype
 * @property \Illuminate\Database\Eloquent\Collection|\App\Option[] $options
 * @property-read \App\RentType $renttype
 * @property-write mixed $decline_types
 * @method static \Illuminate\Database\Query\Builder|\App\Advert advert($advert_id)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert advertList()
 * @method static \Illuminate\Database\Query\Builder|\App\Advert defaultSort()
 * @method static \Illuminate\Database\Query\Builder|\App\Advert findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert mainAdverts()
 * @method static \Illuminate\Database\Query\Builder|\App\Advert myAdverts($user)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert myLastAdvert($user)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert whereDistrictId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert whereFloor($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert whereImages($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert whereObjTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert wherePhoneNum($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert wherePublicated($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert whereRentTypeId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert whereRoomNum($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert whereSquare($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert whereTitleImg($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Advert whereYandexCoord($value)
 * @mixin \Eloquent
 * @property-read mixed $main_image
 * @property-read mixed $cropped_main_image_url
 * @property-read mixed $formatted_room_num
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\DeclineType[] $declineTypes
 * @property-read \App\ObjType $objType
 * @property-read \App\RentType $rentType
 * @method static \Illuminate\Database\Query\Builder|\App\Advert published()
 * @method static \Illuminate\Database\Query\Builder|\App\Advert sortByDate($direction = 'asc')
 * @method static \Illuminate\Database\Query\Builder|\App\Advert sortByPrice($direction = 'asc')
 * @method static \Illuminate\Database\Query\Builder|\App\Advert sortBySquare($direction = 'asc')
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comment
 * @method static \Illuminate\Database\Query\Builder|\App\Advert anotherAdvertsByRoomNum(\App\Advert $advert)
 * @property-read mixed $limited_title
 * @method static \Illuminate\Database\Query\Builder|\App\Advert my()
 */
class Advert extends Model
{

	use Sluggable;

	const COLUMN_NAME_ID = 'id';
	const COLUMN_NAME_TITLE = 'title';
	const COLUMN_NAME_SLUG = 'slug';
	const COLUMN_NAME_TEXT = 'text';
	const COLUMN_NAME_IMAGES = 'images';
	const COLUMN_NAME_ROOM_NUM = 'room_num';
	const COLUMN_NAME_PRICE = 'price';
	const COLUMN_NAME_OBJ_TYPE_ID = 'obj_type_id';
	const COLUMN_NAME_RENT_TYPE_ID = 'rent_type_id';
	const COLUMN_NAME_SQUARE = 'square';
	const COLUMN_NAME_PHONE_NUM = 'phone_num';
	const COLUMN_NAME_YANDEX_COORD = 'yandex_coord';
	const COLUMN_NAME_USER_ID = 'user_id';
	const COLUMN_NAME_ADDRESS = 'address';
	const COLUMN_NAME_DISTRICT_ID = 'district_id';
	const COLUMN_NAME_FLOOR = 'floor';
	const COLUMN_NAME_OPTION_ID = 'option_id';
	const COLUMN_NAME_FEATURE_ID = 'feature_id';
	const COLUMN_NAME_PUBLICATED = 'publicated';
	const COLUMN_NAME_CREATED_AT = 'created_at';
	const COLUMN_NAME_UPDATED_AT = 'upadted_at';

	protected $fillable = [
		self::COLUMN_NAME_ID,
		self::COLUMN_NAME_TITLE,
		self::COLUMN_NAME_SLUG,
		self::COLUMN_NAME_TEXT,
		self::COLUMN_NAME_IMAGES,
		self::COLUMN_NAME_ROOM_NUM,
		self::COLUMN_NAME_PRICE,
		self::COLUMN_NAME_OBJ_TYPE_ID,
		self::COLUMN_NAME_RENT_TYPE_ID,
		self::COLUMN_NAME_SQUARE,
		self::COLUMN_NAME_PHONE_NUM,
		self::COLUMN_NAME_YANDEX_COORD,
		self::COLUMN_NAME_USER_ID,
		self::COLUMN_NAME_ADDRESS,
		self::COLUMN_NAME_DISTRICT_ID,
		self::COLUMN_NAME_FLOOR,
		self::COLUMN_NAME_OPTION_ID,
		self::COLUMN_NAME_FEATURE_ID,
		self::COLUMN_NAME_PUBLICATED,
	];

	protected $appends = ['main_image', 'cropped_main_image_url', 'formatted_room_num', 'limited_title'];

	protected $hidden = [self::COLUMN_NAME_PHONE_NUM];

	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'title',
			],
		];
	}

	public function scopeSortByDate(Builder $query, $direction = 'desc')
	{
		return $query->orderBy('adverts.' . static::CREATED_AT, $direction);
	}

	public function scopeSortByPrice(Builder $query, $direction = 'asc')
	{
		return $query->orderBy(static::COLUMN_NAME_PRICE, $direction);
	}

	public function scopeSortBySquare(Builder $query, $direction = 'desc')
	{
		return $query->orderBy(static::COLUMN_NAME_SQUARE, $direction);
	}

	public function scopePublished(Builder $query)
	{
		return $query->where(static::COLUMN_NAME_PUBLICATED, '=', 1);
	}

	public static function getMinFloor()
	{
		return Advert::min(Advert::COLUMN_NAME_FLOOR);
	}

	public static function getMaxFloor()
	{
		return Advert::max(Advert::COLUMN_NAME_FLOOR);
	}

	public static function getMinSquare() {
		return Advert::min(Advert::COLUMN_NAME_SQUARE);
	}

	public static function getMaxSquare() {
		return Advert::max(Advert::COLUMN_NAME_SQUARE);
	}

	public static function getMinPrice() {
		return Advert::min(Advert::COLUMN_NAME_PRICE);
	}

	public static function getMaxPrice() {
		return Advert::max(Advert::COLUMN_NAME_PRICE);
	}

//	public function setOptionsAttribute($options)
//	{
//		$this->options()
//			->detach();
//
//		if (!$options) {
//			return;
//		}
//
//		if (!$this->exists) {
//			$this->save();
//		}
//
//		$this->options()
//			->attach($options);
//	}
//
//	public function setFeaturesAttribute($features)
//	{
//		$this->features()
//			->detach();
//
//		if (!$features) return;
//		if (!$this->exists) $this->save();
//
//		$this->features()
//			->attach($features);
//	}
//
//	public function setDeclineTypesAttribute($declineTypes)
//	{
//		$this->declineTypes()
//			->detach();
//
//		if (!$declineTypes) {
//			return;
//		}
//
//		if (!$this->exists) {
//			$this->save();
//		}
//
//		$this->declineTypes()
//			->attach($declineTypes);
//	}

	public function author()
	{
		return $this->belongsTo('App\User', 'user_id');
	}

	public function objType()
	{
		return $this->belongsTo('App\ObjType', 'obj_type_id');
	}

	public function rentType()
	{
		return $this->belongsTo('App\RentType', 'rent_type_id');
	}

	public function district()
	{
		return $this->belongsTo('App\District', 'district_id');
	}

	public function options()
	{
		return $this->belongsToMany('App\Option');
	}

	public function features()
	{
		return $this->belongsToMany('App\Feature');
	}

	public function declineTypes()
	{
		return $this->belongsToMany('App\DeclineType');
	}

	public function comment()
	{
		return $this->hasMany('App\Comment');
	}

	public function getImagesAttribute($value)
	{
		return json_decode($value);
	}

	public function getMainImageAttribute()
	{
		return $this->images[0];
	}

	public function setImagesAttribute($value)
	{
		$this->attributes['images'] = json_encode($value);
	}

	public function scopeMainAdverts($query)
	{
		return $query->published()
			->sortByDate()
			->take(8)
			->get();
	}

	public function scopeAnotherAdvertsByRoomNum(Builder $query, Advert $advert)
	{
		return $query->published()
			->where(static::COLUMN_NAME_ROOM_NUM, $advert->room_num)
			->where(static::COLUMN_NAME_ID, '<>', $advert->id)
			->sortByDate()
			->take(4)
			->get();
	}

	/**
	 * Scope only my adverts
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeMy($query)
	{
		return $query->where('user_id', \Auth::user()->id)
			->sortByDate();
	}

	public function scopeMyLastAdvert($query, $user)
	{
		return $query->where('user_id', $user)
			->sortByDate()
			->first();
	}

	public function scopeAdvertList($query)
	{
		return $query->published()
			->sortByDate()
			->get();
	}

	public function getFormattedRoomNumAttribute()
	{
		if ($this->obj_type_id === 2) {
			return 'К';
		}

		return $this->room_num;
	}

	public function getCroppedMainImageUrlAttribute()
	{
		return Image::url($this->main_image, 700, 500, ['crop']);
	}

	public function getLimitedTitleAttribute()
	{
		return str_limit($this->title, 40);
	}

	public static function boot()
    {
        parent::boot();
		self::updating(function($model){
        	$original = $model->getOriginal();

        	if ($model->declinetypes->count()) {
        		$model->author->notify(new \App\Notifications\DeclineAdvert($model));

        		// Unpublish advert
        		$model->publicated = false;
        	}

        	if ($model->publicated && !$original['publicated']) {
        		$model->author->notify(new \App\Notifications\AcceptAdvert($model));
        	}

        	return $model;
        });
    }
}
