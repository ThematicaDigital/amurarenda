<?php namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Banner
 *
 * @property int $id
 * @property string $title
 * @property string $img
 * @property string $link
 * @property bool $publicated
 * @property string $slug
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Banner defaultSort()
 * @method static \Illuminate\Database\Query\Builder|\App\Banner findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Query\Builder|\App\Banner mainSlider()
 * @method static \Illuminate\Database\Query\Builder|\App\Banner whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Banner whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Banner whereImg($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Banner whereLink($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Banner wherePublicated($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Banner whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Banner whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Banner whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Banner extends Model
{

	use Sluggable;

	//
	// protected $fillable = array('title', 'text', 'phone');

	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'title',
			],
		];
	}

	public function scopeDefaultSort($query)
	{
		return $query->orderBy('created_at', 'desc');
	}

	public function scopeMainSlider($query)
	{
		return $query->where('publicated', true)
			->orderBy('created_at', 'asc')
			->get();
	}

}
