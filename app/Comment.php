<?php namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

/**
 * App\Comment
 *
 * @property int $id
 * @property int $user_id
 * @property string $text
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $slug
 * @property bool $publicated
 * @property-read \App\User $author
 * @method static \Illuminate\Database\Query\Builder|\App\Comment defaultSort()
 * @method static \Illuminate\Database\Query\Builder|\App\Comment findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment wherePublicated($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereUserId($value)
 * @mixin \Eloquent
 * @property int $advert_id
 * @property string $name
 * @property-read \App\Advert $advert
 * @method static \Illuminate\Database\Query\Builder|\App\Comment active()
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereAdvertId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Comment whereName($value)
 */
class Comment extends Model
{
    /**
     * @var array
     */
	protected $fillable = [
        'name',
        'text',
        'user_id',
        'advert_id',
        'publicated',
    ];

    /**
     * @var array
     */
    protected $validationMessages = [
        'required' => 'Поле обязательно для заполнения'
    ];

    /**
     * @var array
     */
    protected $rules = [
        'name' => 'required|string',
        'phone' => 'required|string',
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function($model){
            $model->user_id = Auth::user()->id;
            return $model;
        });
    }

    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDefaultSort($query)
    {
        return $query->orderBy('created_at', 'desc');
    }

    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
	public function scopeActive($query)
	{
		return $query->where('publicated', true);
	}

    /**
     * @var \Illuminate\Database\Eloquent\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * @var \Illuminate\Database\Eloquent\BelongsTo
     */
	public function advert()
	{
		return $this->belongsTo('App\Advert', 'advert_id');
	}
}