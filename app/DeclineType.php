<?php namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\DeclineType
 *
 * @property int $id
 * @property string $name
 * @property string $slug
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Advert[] $adverts
 * @method static \Illuminate\Database\Query\Builder|\App\DeclineType findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Query\Builder|\App\DeclineType whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DeclineType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DeclineType whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DeclineType whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\DeclineType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class DeclineType extends Model
{

	public function adverts()
	{
		return $this->belongsToMany('App\Advert');
	}

}
