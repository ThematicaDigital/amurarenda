<?php namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Feature
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $slug
 * @property string $img
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Advert[] $adverts
 * @method static \Illuminate\Database\Query\Builder|\App\Feature findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Query\Builder|\App\Feature whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Feature whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Feature whereImg($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Feature whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Feature whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Feature whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read mixed $image_without_extension
 */
class Feature extends Model
{
	const COLUMN_NAME_ID = 'id';

	public function adverts()
	{
		return $this->belongsToMany('App\Advert');
	}

	public function getImageWithoutExtensionAttribute($image)
	{
		return str_replace('.svg', '', $this->img);
	}

}
