<?php

namespace App\Http\Controllers;

use App\District;
use App\Advert;
use App\Http\Requests\StoreAdvertRequest;
use App\ObjType;
use App\RentType;
use App\Option;
use App\Feature;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class AdvertController extends Controller
{
	public function index(Request $request)
	{
		$filters = [
			SearchController::FILTER_NAME_SORT         => 1,
			SearchController::FILTER_NAME_CHUNK_SIZE   => 12,
			SearchController::FILTER_NAME_START        => 0,
			SearchController::FILTER_NAME_MAX_PRICE    => Advert::getMaxPrice(),
			SearchController::FILTER_NAME_MIN_PRICE    => Advert::getMinPrice(),
			SearchController::FILTER_NAME_SEARCH_TYPES => [1],
			SearchController::FILTER_NAME_RENT_TYPE_ID => 1,
			SearchController::FILTER_NAME_DISTRICT_ID  => 1,
			SearchController::FILTER_NAME_ROOMS_NUMBER => 1,
		];

		if ($request->has('rent-types')) {
			$filters[SearchController::FILTER_NAME_RENT_TYPE_ID] = $request->input('rent-types');
		}

		if ($request->has('apartment-types')) {
			$filters[SearchController::FILTER_NAME_SEARCH_TYPES] = [$request->input('apartment-types')];
		}

		$adverts = SearchController::getAdvertsByFilters($filters)
			->toJson();

		$filters = json_encode(SearchController::getFiltersData());

		return view('advert-list', compact('adverts', 'filters'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create(Request $request)
	{
		if (!\Auth::check()) {
			$request->session()
				->flash('flash_notification.level', 'error');
			$request->session()
				->flash('flash_notification.message', "Для добавления объявления <a style='color: #337ab7' data-toggle='modal' href='#authModal'>авторизируйтесь</a>");

			return redirect()->route('home');
		}

		$apartmentTypes = ObjType::all();
		$options = Option::all();
		$features = Feature::all();

		$districts = District::all()
			->pluck('name', 'id')
			->all();
		$rentTypes = RentType::all()
			->pluck('name', 'id')
			->all();

		$roomsCount = range(1, config('apartments.rooms.max'));

		return view('new-advert', compact('apartmentTypes', 'rentTypes', 'options', 'features', 'districts', 'roomsCount'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(StoreAdvertRequest $request)
	{
		$advert = Advert::create(array_merge($request->only([
			Advert::COLUMN_NAME_ID,
			Advert::COLUMN_NAME_TITLE,
			Advert::COLUMN_NAME_SLUG,
			Advert::COLUMN_NAME_TEXT,
			Advert::COLUMN_NAME_IMAGES,
			Advert::COLUMN_NAME_ROOM_NUM,
			Advert::COLUMN_NAME_PRICE,
			Advert::COLUMN_NAME_OBJ_TYPE_ID,
			Advert::COLUMN_NAME_RENT_TYPE_ID,
			Advert::COLUMN_NAME_SQUARE,
			Advert::COLUMN_NAME_PHONE_NUM,
			Advert::COLUMN_NAME_YANDEX_COORD,
			Advert::COLUMN_NAME_ADDRESS,
			Advert::COLUMN_NAME_DISTRICT_ID,
			Advert::COLUMN_NAME_FLOOR,
		]), [
			Advert::COLUMN_NAME_USER_ID    => \Auth::user()->id,
			Advert::COLUMN_NAME_PUBLICATED => 0,
		]));

		$advert->options()
			->attach(collect($request->input('options'))->keys());
		$advert->features()
			->attach(collect($request->input('features'))->keys());

		$request->session()
			->flash('flash_notification.level', 'success');
		$request->session()
			->flash('flash_notification.message', 'Ваше объявление отправлено на модерацию');

		return redirect()->route('user.adverts');
	}

	public function imageUpload(Request $request)
	{
		$image = $request->file('images');
		$imageName = time() . $image->getClientOriginalName();
		$image->move(public_path('images/uploads'), $imageName);

		return response()->json(['success' => $imageName]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  string $slug
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	public function show($slug)
	{
		$advert = Advert::where('slug', $slug)
			->first();

		if (!$advert) {
			return redirect()
				->route('advert.list')
				->withErrors(['Объявление не найдено']);
		}

		$anotherAdvertsByRoomNum = Advert::anotherAdvertsByRoomNum($advert);

		return view('advert/index', compact('advert', 'anotherAdvertsByRoomNum'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		if (!\Auth::check()) {
			\Session::flash('flash_notification.message', 'Для редактирования объявления <a style=\'color: #337ab7\' data-toggle=\'modal\' href=\'#authModal\'>авторизируйтесь</a>');
			\Session::flash('flash_notification.level', 'error');

			return redirect()->back();
		}

		$advert = Advert::with('options', 'features')
			->find($id);

		if (!$advert) {
			\Session::flash('flash_notification.message', 'Объявление не найдено');
			\Session::flash('flash_notification.level', 'error');

			return redirect()->back();
		}

		if ($advert->user_id != Auth::user()->id) {
			\Session::flash('flash_notification.message', 'Запрещено');
			\Session::flash('flash_notification.level', 'error');

			return redirect()->back();
		}

		$apartmentTypes = ObjType::all();
		$options = Option::all();
		$features = Feature::all();

		$districts = District::all()
			->pluck('name', 'id')
			->all();
		$rentTypes = RentType::all()
			->pluck('name', 'id')
			->all();

		$roomsCount = range(1, config('apartments.rooms.max'));

		return view('new-advert', compact('apartmentTypes', 'rentTypes', 'options', 'features', 'districts', 'roomsCount', 'advert'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param StoreAdvertRequest $request
	 *
	 * @return Response
	 * @internal param int $id
	 *
	 */
	public function update(StoreAdvertRequest $request)
	{
		$advert = Advert::find($request->id);

		if (!$advert) {
			\Session::flash('flash_notification.message', 'Объявление не найдено');
			\Session::flash('flash_notification.level', 'error');

			return redirect()->back();
		}

		if ($advert->user_id != Auth::user()->id) {
			\Session::flash('flash_notification.message', 'Запрещено');
			\Session::flash('flash_notification.level', 'error');

			return redirect()->back();
		}

		$advert->fill(array_merge($request->only([
			Advert::COLUMN_NAME_ID,
			Advert::COLUMN_NAME_TITLE,
			Advert::COLUMN_NAME_SLUG,
			Advert::COLUMN_NAME_TEXT,
			Advert::COLUMN_NAME_IMAGES,
			Advert::COLUMN_NAME_ROOM_NUM,
			Advert::COLUMN_NAME_PRICE,
			Advert::COLUMN_NAME_OBJ_TYPE_ID,
			Advert::COLUMN_NAME_RENT_TYPE_ID,
			Advert::COLUMN_NAME_SQUARE,
			Advert::COLUMN_NAME_PHONE_NUM,
			Advert::COLUMN_NAME_YANDEX_COORD,
			Advert::COLUMN_NAME_ADDRESS,
			Advert::COLUMN_NAME_DISTRICT_ID,
			Advert::COLUMN_NAME_FLOOR,
		]), [
			Advert::COLUMN_NAME_USER_ID    => \Auth::user()->id,
			Advert::COLUMN_NAME_PUBLICATED => 0,
		]));

		$advert->save();

		$advert->options()
			->sync(collect($request->input('options'))->keys());
		$advert->features()
			->sync(collect($request->input('features'))->keys());

		$request->session()
			->flash('flash_notification.level', 'success');
		$request->session()
			->flash('flash_notification.message', 'Ваше объявление отправлено на модерацию');

		return redirect()->route('user.adverts');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function delete($id)
	{
		if (!Auth::check()) {
			\Session::flash('flash_notification.message', 'Необходимо войти на сайт');
			\Session::flash('flash_notification.level', 'error');

			return redirect()->back();
		}

		$advert = Advert::find($id);

		if (!$advert) {
			\Session::flash('flash_notification.message', 'Объявление не найдено');
			\Session::flash('flash_notification.level', 'error');

			return redirect()->back();
		}

		if ($advert->user_id != Auth::user()->id) {
			\Session::flash('flash_notification.message', 'Запрещено');
			\Session::flash('flash_notification.level', 'error');

			return redirect()->back();
		}

		$advert->delete();

		\Session::flash('flash_notification.message', 'Объявление успешно удалено');
		\Session::flash('flash_notification.level', 'success');

		return redirect()->back();
	}

	/**
	 * Show phone
	 *
	 * @param \Illuminate\Http\Request $$request
	 *
	 * @return Response
	 */
	public function showPhone(Request $request)
	{
		$json = [
			'modal'   => '#authModal',
			'message' => 'Необходимо авторизоваться',
			'type'    => 'error',
		];
		if (Auth::check()) {
			$user = Auth::user();
			$json = [
				'message'  => 'Необходимо выбрать тарифный план',
				'type'     => 'error',
				'redirect' => route('pricing'),
			];
			if ($user->tariff_id) {
				$tariff = \App\Tariff::where('id', $user->tariff_id)
					->first();
				if ($tariff->days) {
					$now = Carbon::now();
					$start = Carbon::parse($user->tariff_start);
					$end = Carbon::parse($user->tariff_start)
						->addDays($tariff->days);
					$json = [
						'message' => 'Срок действия тарифного плана истек',
						'type'    => 'error',
					];
					if ($now->between($start, $end)) {
						if ($request->id) {
							$advert = Advert::where('id', $request->id)
								->first();
							$json = [
								'message' => 'Объявление не найдено',
								'type'    => 'error',
							];
							if ($advert) {
								$json = [
									'status' => true,
									'phone'  => $advert->phone_num,
								];
							}
						}
					}
				} else {
					$json = [
						'message'  => 'Пожалуйста, заполните форму',
						'type'     => 'success',
						'redirect' => route('pricing'),
					];
				}
			}
		}

		return response()->json($json);
	}
}
