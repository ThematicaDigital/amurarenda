<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Session;
use \App\Comment;

class CommentController extends Controller
{
	/**
	 * Get comments
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function getComments(Request $request)
	{
		$comments = Comment::active()
			->with('author')
			->where('advert_id', $request->advert_id)
			->skip($request->input('start'))
			->take($request->input('limit'))
			->get();
		$count = Comment::active()->where('advert_id', $request->advert_id)
			->count();
		return response()->json([
			'comments' => $comments,
			'count' => $count
		]);
	}

	/**
	 * Save new comment
	 * @param Request $request
	 * @return Response
	 */
	public function sendComment(Request $request)
	{
		if (!Auth::check()) {
			Session::flash('flash_notification.message', 'Необходимо войти на сайт');
        	Session::flash('flash_notification.level', 'error');
			return redirect()->back();
		}
		if ($request->isMethod('post')) {
            $comment = new Comment($request->all());
            if (!$comment->save()) {
                return redirect()->withErrors($comment->getErrors())->back();
            }
        }
        Session::flash('flash_notification.message', 'Отзыв успешно отправлен на модерацию');
    	Session::flash('flash_notification.level', 'success');
        return redirect()->back();
	}
}