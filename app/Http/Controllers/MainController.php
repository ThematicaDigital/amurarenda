<?php namespace App\Http\Controllers;

use App\Banner;
use App\Advert;
use App\ObjType;
use App\RentType;

class MainController extends Controller
{

	public function index()
	{
		$banners = Banner::mainSlider();
		$adverts = Advert::with('objType', 'rentType')
			->mainAdverts();

		$rentTypes = RentType::orderBy(RentType::COLUMN_NAME_ID)->get(['name'])->pluck('name')->toArray();
		array_unshift($rentTypes, "Выберите тип аренды");
		$apartmentTypes = ObjType::all();
		return view('home/index', compact('adverts', 'banners', 'rentTypes', 'apartmentTypes'));
	}

}
