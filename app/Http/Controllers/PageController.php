<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Page;

class PageController extends Controller
{
	/**
	 * Show page
	 *
	 * @param string $slug
	 * @return Response
	 */
	public function show($slug)
	{
		$page = Page::where('slug', $slug)->first();

		if (!$page) {
			\Session::flash('flash_notification.message', 'Страница не найдена');
        	\Session::flash('flash_notification.level', 'error');
			return redirect()->back();
		}

		return view('page', compact('page'));
    }

    /**
	 * Show page
	 *
	 * @param string $slug
	 * @return Response
	 */
    public static function preview($slug)
    {
    	$page = Page::where('slug', $slug)->first();
		return view('page-preview', compact('page'));
    }
}
