<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Idma\Robokassa\Payment as Robokassa;
use Illuminate\Support\Facades\Auth;
use Session;
use App\Tariff;
use App\Payment;
use Carbon\Carbon;

class RobokassaController extends Controller
{
	/**
	 * Robokassa
	 *
	 * @var \Idma\Robokassa\Payment
	 */
	private $_robokassa;

	/**
	 * Instantiate a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->_robokassa = new Robokassa(
		    env('ROBOKASSA_LOGIN'),
		    env('ROBOKASSA_PASS_1'),
		    env('ROBOKASSA_PASS_2'),
		    env('ROBOKASSA_IS_TEST')
		);
	}

	/**
	 * Payment
	 *
	 * @param integer $tariff_id
	 *
	 * @return void
	 */
    public function payment($tariff_id)
    {
    	if (!Auth::check()) {
			Session::flash('flash_notification.message', 'Необходимо войти на сайт');
        	Session::flash('flash_notification.level', 'error');
			return redirect()->back();
		}

		if (!$tariff_id) {
			Session::flash('flash_notification.message', 'Необходимо выбрать тарифный план');
        	Session::flash('flash_notification.level', 'error');
			return redirect()->back();
		}

		$tariff = Tariff::where('id', $tariff_id)->first();

		if (!$tariff) {
			Session::flash('flash_notification.message', 'Необходимо выбрать тарифный план');
        	Session::flash('flash_notification.level', 'error');
			return redirect()->back();
		}

		$payment = new Payment;
		$payment->sum = $tariff->price;
		$payment->client_id = Auth::user()->id;
		$payment->tariff_id = $tariff->id;
		$payment->save();

		$this->_robokassa
		    ->setInvoiceId($payment->id)
		    ->setSum($payment->sum)
		    ->setDescription("Оплата тарифного плана {$tariff->title}");

		return redirect($this->_robokassa->getPaymentUrl());
    }

    /**
     * Result payment
     *
     * @param  Request $request
     * @return Response
     */
    public function result(Request $request)
    {
		if ($this->_robokassa->validateResult($request->toArray())) {
		    $payment = Payment::where('id', $this->_robokassa->getInvoiceId())->first();

		    if ($this->_robokassa->getSum() == $order->sum) {
		    	$payment->status = Payment::STATUS_DONE;
		    	$payment->save();

		    	$user = User::where('id', $payment->client_id)->first();
		    	$user->tariff_id = $payment->tariff_id;
		    	$user->tariff_start = Carbon::now()->toDateString();
		    	$user->save();

		    	return response($this->_robokassa->getSuccessAnswer(), 200)
          			->header('Content-Type', 'text/plain');
		    }

		    $payment->status = Payment::STATUS_FAILED;
		    $payment->save();
		}

		return response('bad sign', 200)
          ->header('Content-Type', 'text/plain');
    }

    /**
     * Success payment
     *
     * @param  Request $request
     * @return Response
     */
    public function success(Request $request)
    {
    	if ($payment->validateSuccess($request->toArray())) {
		    $payment = Payment::where('id', $this->_robokassa->getInvoiceId())->first();
		    if ($payment->getSum() == $order->sum) {
		        Session::flash('flash_notification.message', 'Оплата тарифного плана успешно завершена');
		    	Session::flash('flash_notification.level', 'success');
		    	return redirect()->route('pricing');
		    }

		}
		Session::flash('flash_notification.message', 'Произошла ошибка при оплате тарифного плана');
    	Session::flash('flash_notification.level', 'error');
    	return redirect()->route('pricing');
    }

    /**
     * Failure payment
     *
     * @param  Request $request
     * @return Response
     */
    public function failure(Request $request)
    {
    	Session::flash('flash_notification.message', 'Произошла ошибка при оплате тарифного плана');
    	Session::flash('flash_notification.level', 'error');
    	return redirect()->route('pricing');
    }
}
