<?php

namespace App\Http\Controllers;

use App\Advert;
use App\ObjType;
use App\RentType;
use App\District;
use App\Option;
use App\Feature;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class SearchController extends Controller
{

	const FILTER_NAME_OPTIONS = 'options';
	const FILTER_NAME_SEARCH_TYPES = 'searchTypes';
	const FILTER_NAME_FEATURES = 'features';
	const FILTER_NAME_MIN_SQUARE = 'minSquare';
	const FILTER_NAME_MAX_SQUARE = 'maxSquare';
	const FILTER_NAME_MAX_FLOOR = 'maxFloor';
	const FILTER_NAME_MIN_FLOOR = 'minFloor';
	const FILTER_NAME_MIN_PRICE = 'minPrice';
	const FILTER_NAME_MAX_PRICE = 'maxPrice';
	const FILTER_NAME_START = 'start';
	const FILTER_NAME_CHUNK_SIZE = 'chunkSize';
	const FILTER_NAME_SORT = 'sort';
	const FILTER_NAME_RENT_TYPE_ID = 'rentTypeId';
	const FILTER_NAME_DISTRICT_ID = 'districtId';
	const FILTER_NAME_ROOMS_NUMBER = 'roomsNumber';

	const SORT_ID_BY_DATE = 1;
	const SORT_ID_BY_PRICE = 2;
	const SORT_ID_BY_SQUARE = 3;

	static $mapSortIdToMethod = [
		self::SORT_ID_BY_DATE   => 'sortByDate',
		self::SORT_ID_BY_PRICE  => 'sortByPrice',
		self::SORT_ID_BY_SQUARE => 'sortBySquare',
	];

	public static function getFiltersData()
	{
		return [
			'objectTypes' => ObjType::all(),
			'districts'   => District::all(),
			'rentTypes'   => RentType::all(),
			'options'     => Option::all(),
			'features'    => Feature::all(),
			'minPrice'    => Advert::getMinPrice(),
			'maxPrice'    => Advert::getMaxPrice(),
		];
	}

	public static function getAdvertsByFilters(array $filters)
	{
		$searchTypes = isset($filters[static::FILTER_NAME_SEARCH_TYPES]) ? $filters[static::FILTER_NAME_SEARCH_TYPES] : [];
		$options = isset($filters[static::FILTER_NAME_OPTIONS]) ? $filters[static::FILTER_NAME_OPTIONS] : null;
		$features = isset($filters[static::FILTER_NAME_FEATURES]) ? $filters[static::FILTER_NAME_FEATURES] : null;

		$minSquare = static::getFilterValueOrDefault(static::FILTER_NAME_MIN_SQUARE, $filters);
		$maxSquare = static::getFilterValueOrDefault(static::FILTER_NAME_MAX_SQUARE, $filters);

		$minFloor = static::getFilterValueOrDefault(static::FILTER_NAME_MIN_FLOOR, $filters);
		$maxFloor = static::getFilterValueOrDefault(static::FILTER_NAME_MAX_FLOOR, $filters);

		/** @var Builder $advertsQuery */
		$advertsQuery = Advert::published()
			->where(Advert::COLUMN_NAME_PRICE, '>=', $filters[static::FILTER_NAME_MIN_PRICE])
			->when(!in_array(2, $searchTypes), function (Builder $query) use ($filters) {
				if ($filters[static::FILTER_NAME_ROOMS_NUMBER] >= 4) {
					$query->where(Advert::COLUMN_NAME_ROOM_NUM, '>=', 4);
				} else {
					$query->where(Advert::COLUMN_NAME_ROOM_NUM, '=', $filters[static::FILTER_NAME_ROOMS_NUMBER]);
				}
			})
			->where(Advert::COLUMN_NAME_PRICE, '<=', $filters[static::FILTER_NAME_MAX_PRICE])
			->where(Advert::COLUMN_NAME_RENT_TYPE_ID, '=', $filters[static::FILTER_NAME_RENT_TYPE_ID])
			->where(Advert::COLUMN_NAME_DISTRICT_ID, '=', $filters[static::FILTER_NAME_DISTRICT_ID])
			->where(Advert::COLUMN_NAME_SQUARE, '>=', $minSquare)
			->where(Advert::COLUMN_NAME_SQUARE, '<=', $maxSquare)
			->where(Advert::COLUMN_NAME_FLOOR, '>=', $minFloor)
			->where(Advert::COLUMN_NAME_FLOOR, '<=', $maxFloor)
			->when(isset($filters[static::FILTER_NAME_SORT]) && !empty($filters[static::FILTER_NAME_SORT]), function (Builder $query) use ($filters) {
				$query->{static::$mapSortIdToMethod[$filters[static::FILTER_NAME_SORT]]}();
			})
			->when(!empty($options) && empty($features), function (Builder $query) use ($options) {
				$query->join('advert_option', 'adverts.id', '=', 'advert_option.advert_id')
					->join('options', 'advert_option.option_id', '=', 'options.id')
					->whereIn('options.id', $options)
					->groupBy('adverts.id')
					->havingRaw('COUNT(*) = ' . count($options));
			})
			->when(!empty($features) && empty($options), function (Builder $query) use ($features) {
				$query->join('advert_feature', 'advert_feature.advert_id', '=', 'adverts.id')
					->join('features', 'advert_feature.feature_id', '=', 'features.id')
					->whereIn('features.id', $features)
					->groupBy('adverts.id')
					->havingRaw('COUNT(*) = ' . count($features));
			})
			->when(!empty($features) && !empty($options), function (Builder $query) use ($features, $options) {
				$query->join('advert_option', 'adverts.id', '=', 'advert_option.advert_id')
					->join('options', 'advert_option.option_id', '=', 'options.id')
					->join('advert_feature', 'advert_feature.advert_id', '=', 'adverts.id')
					->join('features', 'advert_feature.feature_id', '=', 'features.id')
					->whereIn('options.id', $options)
					->whereIn('features.id', $features)
					->groupBy('adverts.id')
					->havingRaw('COUNT(DISTINCT feature_id) = ' . count($features) . ' AND COUNT(DISTINCT option_id) = ' . count($options));
			})
			->whereIn(Advert::COLUMN_NAME_OBJ_TYPE_ID, $searchTypes)
			->when(isset($filters[static::FILTER_NAME_START]), function (Builder $query) use ($filters) {
				$query->skip($filters[static::FILTER_NAME_START]);
			})
			->when(isset($filters[static::FILTER_NAME_CHUNK_SIZE]), function (Builder $query) use ($filters) {
				$query->take($filters[static::FILTER_NAME_CHUNK_SIZE]);
			})
			->with('objType');

		return $advertsQuery->get();
	}

	public function getAdverts(Request $request)
	{
		$filters = $request->all();

		return response()->json($this->getAdvertsByFilters($filters));
	}

	public static function getFilterValueOrDefault($filterName, array $filters)
	{
		return isset($filters[$filterName]) && !empty($filters[$filterName])
			? $filters[$filterName]
			: call_user_func(Advert::class . "::get$filterName");
	}
}
