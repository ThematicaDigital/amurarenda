<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Tariff;
use App\Page;
use App\Request as TariffRequest;

class TariffController extends Controller
{

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function choose()
	{
		$tariffs = Tariff::get();

		$page = Page::where('slug', 'tarifnye-plany')->first();

		return view('choose-tariff', compact('tariffs', 'page'));
	}

	/**
	 * [request description]
	 * @param Request $request
	 * @return Response
	 */
	public function request(Request $request)
	{
		if ($request->isMethod('post')) {
            $tariffRequest = new TariffRequest($request->all());
            if (!$tariffRequest->save()) {
                return redirect()->withErrors($tariffRequest->getErrors())->route('pricing');
            }
            \Session::flash('flash_notification.message', 'Заявка успешно отправлена');
			\Session::flash('flash_notification.level', 'success');
        }
        return redirect()->route('pricing');
	}
}