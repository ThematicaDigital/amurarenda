<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use \App\Advert;
use \App\Payment;

class UserController extends Controller
{
    /**
     * Get adverts list
     * @return Response
     */
	public function getAdverts()
	{
		$adverts = Advert::my()
			->with('declinetypes', 'objtype')
			->get();

		return view('user.user-adverts', compact('adverts'));
    }

    /**
     * Get notifications list
     * @return Response
     */
    public function notifications()
    {
    	$notifications = \Auth::user()->notifications;
    	return view('user.notifications', compact('notifications'));
    }

    /**
     * Get payments list
     * @return Response
     */
    public function payments()
    {
    	$payments = Payment::my()->get();
    	return view('user.payments', compact('payments'));
    }
}
