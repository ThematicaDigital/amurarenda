<?php

namespace App\Http\Requests;

use App\Advert;
use Illuminate\Foundation\Http\FormRequest;

class StoreAdvertRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
	        Advert::COLUMN_NAME_OBJ_TYPE_ID => 'required',
            Advert::COLUMN_NAME_TITLE => 'required',
	        Advert::COLUMN_NAME_TEXT => 'required',
	        Advert::COLUMN_NAME_ADDRESS => 'required',
	        Advert::COLUMN_NAME_DISTRICT_ID => 'required',
	        Advert::COLUMN_NAME_RENT_TYPE_ID => 'required',
	        Advert::COLUMN_NAME_ROOM_NUM => 'required',
	        Advert::COLUMN_NAME_YANDEX_COORD => 'required',

	        Advert::COLUMN_NAME_SQUARE => 'required|numeric',
	        Advert::COLUMN_NAME_FLOOR => 'required|numeric',
	        Advert::COLUMN_NAME_PRICE => 'required|numeric',

	        Advert::COLUMN_NAME_PHONE_NUM => 'required',
        ];
    }
}
