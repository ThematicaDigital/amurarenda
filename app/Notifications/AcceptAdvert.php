<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AcceptAdvert extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Advert
     * @var \App\Advert
     */
    protected $advert;

    /**
     * Create a new notification instance.
     *
     * @param \App\Advert $advert
     * @return void
     */
    public function __construct($advert)
    {
        $this->advert = $advert;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->action('Ваше объявление прошло модерацию:', route('advert.show', [$this->advert->slug]))
            ->action('Мои объявления:', route('user.adverts'));
    }

    /**
     * Get the database representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\DatabaseMessage
     */
    public function toDatabase($notifiable)
    {
        return [
            'message' => 'Ваше объявление прошло модерацию:',
            'action' => route('advert.show', [$this->advert->slug], false)
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'message' => 'Ваше объявление прошло модерацию:',
            'action' => route('advert.show', [$this->advert->slug])
        ];
    }
}
