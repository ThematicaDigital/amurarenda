<?php namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\ObjType
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $slug
 * @method static \Illuminate\Database\Query\Builder|\App\ObjType findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Query\Builder|\App\ObjType whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ObjType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ObjType whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ObjType whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\ObjType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ObjType extends Model
{
	use Sluggable;

	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'name'
			]
		];
	}
}
