<?php namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;

/**
 * App\Option
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $slug
 * @property string $img
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Advert[] $adverts
 * @method static \Illuminate\Database\Query\Builder|\App\Option findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Query\Builder|\App\Option whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Option whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Option whereImg($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Option whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Option whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Option whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read mixed $image_without_extension
 */
class Option extends Model
{
	const COLUMN_NAME_ID = 'id';

	public function getImageFields()
	{
		return [
			'img'   => '',
			'photo' => '',
			'other' => [
				'other_images/',
				function ($directory, $originalName, $extension) {
					return $originalName;
				},
			],
		];
	}

	public function adverts()
	{
		return $this->belongsToMany(Advert::class);
	}

	public function getImageWithoutExtensionAttribute($image)
	{
		return str_replace('.svg', '', $this->img);
	}
}
