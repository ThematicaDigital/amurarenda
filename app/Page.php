<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Page
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property string $text
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Page findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Query\Builder|\App\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Page whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Page whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Page whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Page whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Page whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Page extends Model
{
	use Sluggable;

	/**
	 * @var array
	 */
    protected $fillable = [
        'title',
        'slug',
        'text',
    ];

    /**
	 * @var array
	 */
    protected $validationMessages = [
        'required' => 'Поле обязательно для заполнения'
    ];

    /**
	 * @var array
	 */
    protected $rules = [
        'title' => 'required|string',
        'text' => 'required|string',
    ];

    /**
     * Sluggable options
     *
     * @return array
     */
    public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'title',
			],
		];
	}
}
