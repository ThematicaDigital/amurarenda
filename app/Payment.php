<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Payment
 *
 * @property int $id
 * @property int $sum
 * @property int $client_id
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read string $status_title
 * @method static \Illuminate\Database\Query\Builder|\App\Payment whereClientId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payment whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payment whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payment whereStatus($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payment whereSum($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Payment whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Payment extends Model
{
	const STATUS_NEW = 'new';
	const STATUS_PROCESSING = 'processing';
	const STATUS_FAILED = 'failed';
	const STATUS_DONE = 'done';

	/**
	 * @var array
	 */
	protected $fillable = [
		'sum',
		'client_id',
		'status'
	];

	/**
	 * @var array
	 */
	protected $appends = [
		'status_title'
	];

	public function client()
	{
		return $this->belongsTo('App\User', 'client_id');
	}

	public function tariff()
	{
		return $this->belongsTo('App\Tariff', 'tariff_id');
	}

	/**
	 * Get status title
	 * @return string
	 */
    public function getStatusTitleAttribute()
	{
		switch ($this->status) {
			case 'new': return 'Создано'; break;
			case 'processing': return 'В обработке'; break;
			case 'failed': return 'Ошибка'; break;
			case 'done': return 'Оплачено'; break;
		}
		return '';
	}

	/**
	 * Scope only my payments
	 *
	 * @param \Illuminate\Database\Eloquent\Builder $query
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeMy($query)
	{
		return $query->where('client_id', \Auth::user()->id);
	}
}
