<?php

namespace App\Providers;

use SleepingOwl\Admin\Providers\AdminSectionsServiceProvider as ServiceProvider;

class AdminSectionsServiceProvider extends ServiceProvider
{

    /**
     * @var array
     */
    protected $sections = [
	    \App\ObjType::class => 'App\Admin\Sections\ObjTypeSection',
	    \App\RentType::class => 'App\Admin\Sections\RentTypeSection',
	    \App\District::class => 'App\Admin\Sections\DistrictSection',
	    \App\Feature::class => 'App\Admin\Sections\FeatureSection',
	    \App\Option::class => 'App\Admin\Sections\OptionSection',
	    \App\User::class => 'App\Admin\Sections\UserSection',
	    \App\DeclineType::class => 'App\Admin\Sections\DeclineTypeSection',
	    \App\Advert::class => 'App\Admin\Sections\AdvertSection',
        \App\Banner::class => 'App\Admin\Sections\BannerSection',
        \App\Tariff::class => 'App\Admin\Sections\TariffSection',
        \App\Request::class => 'App\Admin\Sections\RequestSection',
        \App\Payment::class => 'App\Admin\Sections\PaymentSection',
        \App\Comment::class => 'App\Admin\Sections\CommentSection',
	    \App\Page::class => 'App\Admin\Sections\PageSection',
    ];

    /**
     * Register sections.
     *
     * @return void
     */
    public function boot(\SleepingOwl\Admin\Admin $admin)
    {
    	//

        parent::boot($admin);
    }
}
