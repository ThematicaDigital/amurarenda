<?php namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * App\RentType
 *
 * @property int $id
 * @property string $name
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $slug
 * @method static \Illuminate\Database\Query\Builder|\App\RentType findSimilarSlugs(\Illuminate\Database\Eloquent\Model $model, $attribute, $config, $slug)
 * @method static \Illuminate\Database\Query\Builder|\App\RentType whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RentType whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RentType whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RentType whereSlug($value)
 * @method static \Illuminate\Database\Query\Builder|\App\RentType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RentType extends Model
{
	const  COLUMN_NAME_ID = 'id';
}
