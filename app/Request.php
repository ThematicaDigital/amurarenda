<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Mail;

/**
 * App\Request
 *
 * @property int $id
 * @property string $name
 * @property string $phone
 * @property string $email
 * @property string $text
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Request whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Request whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Request whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Request whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Request wherePhone($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Request whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Request whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Request extends Model
{
    protected $fillable = [
        'name',
        'phone',
        'email',
        'text',
    ];

    protected $validationMessages = [
        'required' => 'Поле обязательно для заполнения'
    ];

    protected $rules = [
        'name' => 'required|string',
        'phone' => 'required|string',
    ];

    public static function boot()
    {
        parent::boot();
        self::creating(function($model){
            // Mail::send('emails.request', ['request' => $model], function ($m) use ($model) {
            //     $m->from('support@amurarenda.ru', 'Амураренда');
            //     $m->to('support@amurarenda.ru', 'Амураренда')->subject('Новая заявка!');
            // });
            return $model;
        });
    }
}
