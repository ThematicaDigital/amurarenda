<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Tariff
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $price
 * @property int $price_old
 * @property int $days
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $user
 * @method static \Illuminate\Database\Query\Builder|\App\Tariff whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tariff whereDays($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tariff whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tariff whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tariff wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tariff wherePriceOld($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tariff whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Tariff whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Tariff extends Model
{
	/**
	 * @var array
	 */
	protected $fillable = [
		'title',
		'description'
	];

	/**
	 * Get user
	 *
	 * @return \Illuminate\Database\Eloquent\Relations\HasMany
	 */
	public function user()
	{
		return $this->hasMany('App\User');
	}

	public function payment()
	{
		return $this->hasMany('App\Payment');
	}
}
