<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use App\Comment;

/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $remember_token
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Advert[] $advert
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comment
 * @method static \Illuminate\Database\Query\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $role_id
 * @property-read \App\Role $role
 * @method static \Illuminate\Database\Query\Builder|\App\User whereRoleId($value)
 * @property int $tariff_id
 * @property string $tariff_start
 * @property-read \App\Tariff $tariff
 * @method static \Illuminate\Database\Query\Builder|\App\User whereTariffId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User whereTariffStart($value)
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 */
class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{

	use Authenticatable, CanResetPassword, Notifiable;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function comment()
	{
		return $this->hasMany('App\Comment');
	}

	public function advert()
	{
		return $this->hasMany('App\Advert');
	}

	public function role()
	{
		return $this->belongsTo('App\Role', 'role_id');
	}

	public function isAdmin()
	{
		return $this->role_id == \App\Role::ROLE_ADMIN;
	}

	public function tariff()
	{
		return $this->belongsTo('App\Tariff', 'tariff_id');
	}

	public function payments()
	{
		return $this->hasMany('App\Payment');
	}
}
