<?php
return [
	'images-width'  => 800,
	'images-height' => 800,
	'images-path'   => base_path() . '/public/images/uploads',
	'images-count'  => 20,
	'category'      => 'cats',

	'excluded' => [
		'.gitignore',
	],
];
