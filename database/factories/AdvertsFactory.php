<?php

use ScaredChatsky\ImageSeeder\Facades\ImageSeeder;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Advert::class, function (Faker\Generator $faker) {
	$storage = public_path(config('sleeping_owl.imagesUploadDirectory'));

	$images = ImageSeeder::getRandomImages(random_int(2, 8), '/images/uploads/');

    $faker = Faker\Factory::create('ru_RU');

	return [
		'title' => $faker->words($faker->numberBetween(3, 10), true),
    	'text' => $faker->text(),
    	'images' => $images,
    	'room_num' => $faker->numberBetween(1, 10),
    	'price' => $faker->numberBetween(1000, 10000),
    	'obj_type_id' => App\ObjType::inRandomOrder()->first()->id,
    	'rent_type_id' => App\RentType::inRandomOrder()->first()->id,
        'square' => $faker->numberBetween(10, 80),
        'phone_num' => $faker->phoneNumber(),
        'yandex_coord' => $faker->latitude(50.20, 50.59) . ',' . $faker->longitude(127.51, 127.56),
        'publicated' => $faker->boolean(90),
        'user_id' => App\User::inRandomOrder()->first()->id,
        'address' => $faker->streetAddress,
        'district_id' => App\District::inRandomOrder()->first()->id,
        'floor' => $faker->numberBetween(1, 10),
	];
});
