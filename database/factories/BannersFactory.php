<?php

use ScaredChatsky\ImageSeeder\Facades\ImageSeeder;

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Banner::class, function (Faker\Generator $faker) {
	$storage = public_path(config('sleeping_owl.imagesUploadDirectory'));

	$images = ImageSeeder::getRandomImages(random_int(2, 8), '/images/uploads/');

	return [
		'title'      => $faker->sentence(rand(4, 6)),
		'img'        => str_replace(public_path(), '', $faker->randomElement($images)),
		'link'       => $faker->url(),
		'publicated' => true,
	];
});
