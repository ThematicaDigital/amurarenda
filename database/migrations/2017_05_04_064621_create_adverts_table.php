<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('adverts', function (Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->longText('text');
			$table->longText('images');
			$table->integer('room_num');
			$table->bigInteger('price');
			$table->integer('obj_type_id');
			$table->integer('rent_type_id');
			$table->integer('square');
			$table->string('phone_num');
			$table->string('yandex_coord');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('adverts');
	}

}
