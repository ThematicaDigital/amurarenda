<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveSlugs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
        Schema::table('decline_types', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
        Schema::table('districts', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
        Schema::table('features', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
        Schema::table('obj_types', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
        Schema::table('options', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
        Schema::table('rent_types', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->string('slug');
        });
        Schema::table('decline_types', function (Blueprint $table) {
            $table->string('slug');
        });
        Schema::table('districts', function (Blueprint $table) {
            $table->string('slug');
        });
        Schema::table('features', function (Blueprint $table) {
            $table->string('slug');
        });
        Schema::table('obj_types', function (Blueprint $table) {
            $table->string('slug');
        });
        Schema::table('options', function (Blueprint $table) {
            $table->string('slug');
        });
        Schema::table('rent_types', function (Blueprint $table) {
            $table->string('slug');
        });
    }
}
