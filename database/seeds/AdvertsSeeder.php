<?php

use Illuminate\Database\Seeder;
use App\Advert;

class AdvertsSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Advert::truncate();

		DB::table('advert_option')
			->truncate();

		DB::table('advert_feature')
			->truncate();

		$optionsIds = \App\Option::all([\App\Option::COLUMN_NAME_ID])
			->pluck(\App\Option::COLUMN_NAME_ID)
			->toArray();

		$featuresIds = \App\Feature::all([\App\Option::COLUMN_NAME_ID])
			->pluck(\App\Feature::COLUMN_NAME_ID)
			->toArray();

		$faker = \Faker\Factory::create();

		factory(Advert::class, 200)
			->create()
			->each(function (Advert $advert) use ($optionsIds, $featuresIds, $faker) {
				$advert->options()
					->attach($faker->randomElements($optionsIds, rand(0, count($optionsIds))));

				$advert->features()
					->attach($faker->randomElements($featuresIds, rand(0, count($featuresIds))));
			});
	}
}
