<?php

use Illuminate\Database\Seeder;
use App\Banner;

class BannersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Banner::truncate();

        factory(Banner::class, 3)->create();
    }
}
