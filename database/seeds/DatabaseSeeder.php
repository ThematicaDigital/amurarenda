<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//File::delete(File::allFiles(public_path('images/uploads')));

		$this->call(TariffsSeeder::class);
		$this->call(RolesSeeder::class);
		$this->call(UsersSeeder::class);
		$this->call(ObjTypesSeeder::class);
		$this->call(RentTypesSeeder::class);
		$this->call(OptionsSeeder::class);
		$this->call(FeaturesSeeder::class);
		$this->call(DistrictsSeeder::class);
		$this->call(DeclineTypesSeeder::class);
		// $this->call(BannersSeeder::class);
		// $this->call(AdvertsSeeder::class);
		$this->call(PagesSeeder::class);
	}
}
