<?php

use Illuminate\Database\Seeder;
use App\DeclineType;

class DeclineTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    DeclineType::truncate();

        DeclineType::create(['name' => 'Недостаточно фотографий']);
        DeclineType::create(['name' => 'Присутсвует нецензурная брань']);
        DeclineType::create(['name' => 'Некорректный адрес']);
    }
}
