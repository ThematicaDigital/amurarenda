<?php

use Illuminate\Database\Seeder;
use App\District;

class DistrictsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    District::truncate();

        District::create(['name' => '1-й микрорайон']);
        District::create(['name' => '2-й микрорайон']);
        District::create(['name' => '3-й микрорайон']);
    }
}
