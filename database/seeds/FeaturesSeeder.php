<?php

use Illuminate\Database\Seeder;
use App\Feature;

class FeaturesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    Feature::truncate();

        Feature::create(['name' => 'Можно курить', 'img' => 'images/air-conditioner.svg']);
        Feature::create(['name' => 'Подходит для мероприятий', 'img' => 'images/air-conditioner.svg']);
        Feature::create(['name' => 'Можно с животными', 'img' => 'images/air-conditioner.svg']);
        Feature::create(['name' => 'Можно с детьми', 'img' => 'images/air-conditioner.svg']);
    }
}
