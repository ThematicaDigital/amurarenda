<?php

use Illuminate\Database\Seeder;

class NotificationsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('notifications')->truncate();

        $adverts = \App\Advert::get();

        if ($adverts) {
        	foreach ($adverts as $advert) {
        		if ($advert->publicated) {
        			$advert->author->notify(new \App\Notifications\AcceptAdvert($advert));
        		} else {
        			$advert->author->notify(new \App\Notifications\DeclineAdvert($advert));
        		}
        	}
        }
    }
}
