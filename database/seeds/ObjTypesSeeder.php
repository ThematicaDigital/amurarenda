<?php

use Illuminate\Database\Seeder;
use App\ObjType;

class ObjTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    ObjType::truncate();

        ObjType::create(['name' => 'Коттедж']);
        ObjType::create(['name' => 'Комната']);
        ObjType::create(['name' => 'Квартира']);
    }
}
