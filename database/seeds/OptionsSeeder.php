<?php

use Illuminate\Database\Seeder;
use App\Option;


class OptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    Option::truncate();

        Option::create(['name' => 'Кондиционер', 'img' => 'images/air-conditioner.svg']);
        Option::create(['name' => 'Чайник', 'img' => 'images/boiler.svg']);
        Option::create(['name' => 'Печь', 'img' => 'images/cooker.svg']);
        Option::create(['name' => 'Посудомоечная машина', 'img' => 'images/dishwasher.svg']);
        Option::create(['name' => 'Холодильник', 'img' => 'images/fridge-2.svg']);
        Option::create(['name' => 'Микроволновая печь', 'img' => 'images/microwave.svg']);
        Option::create(['name' => 'Обогреватель', 'img' => 'images/radiator-4.svg']);
        Option::create(['name' => 'Стиральная машина', 'img' => 'images/washing-machine.svg']);
    }
}
