<?php

use Illuminate\Database\Seeder;
use App\Page;

class PagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::truncate();

        Page::create([
        	'title' => 'Пользовательское соглашение',
        	'text' => 'Пользовательское соглашение'
    	]);
        Page::create([
            'title' => 'Оферта',
            'text' => 'Оферта'
        ]);
        Page::create([
        	'title' => 'Тарифные планы',
        	'text' => 'Тарифные планы'
    	]);
    }
}
