<?php

use Illuminate\Database\Seeder;
use App\RentType;

class RentTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	    RentType::truncate();

        RentType::create(['name' => 'Посуточная аренда']);
        RentType::create(['name' => 'Долгосрочная аренда']);
    }
}
