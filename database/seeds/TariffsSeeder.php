<?php

use Illuminate\Database\Seeder;
use App\Tariff;

class TariffsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tariff::truncate();

        Tariff::create([
        	'title' => 'Стандартный',
        	'description' => 'Доступ к базе  с прямыми  контактами собственников',
        	'price' => 1500,
        	'price_old' => 2000,
        	'days' => 5,
    	]);
        Tariff::create([
        	'title' => 'Расширенный',
        	'description' => 'Доступ к базе  с прямыми  контактами собственников',
        	'price' => 3000,
        	'price_old' => 3500,
        	'days' => 15,
    	]);
        Tariff::create([
        	'title' => 'Премиум 30%',
        	'description' => 'Доступ к базе  с прямыми  контактами собственников'
    	]);
    }
}
