<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Role;
use App\Tariff;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tarrif = Tariff::inRandomOrder()->first();

	    User::truncate();

        User::create([
            'email' => 'admin@site.com',
            'password' => bcrypt('password'),
            'role_id' => Role::ROLE_ADMIN,
        ]);

        User::create([
        	'email' => 'user@site.com',
        	'password' => bcrypt('password'),
            'role_id' => Role::ROLE_USER,
            'tariff_id' => $tarrif->id,
	        'tariff_start' => date('Y-m-d H:i:s'),
    	]);
    }
}
