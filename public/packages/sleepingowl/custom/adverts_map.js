var map, placemark, bounds;

ymaps.ready(init);

/**
 * Create placemark on the map
 * @param  {array} coords Coordinates of the center of the placemark
 * @return {void}
 */
function createPlacemark(coords)
{
	$('#yandex_coord').val(coords);
    if (!placemark) {
        map.setZoom(15);
        placemark = new ymaps.Placemark(coords, {}, {iconLayout: 'default#image'});
        map.geoObjects.add(placemark);
        placemark.events.add('dragend', function () {
            getAddress(placemark.geometry.getCoordinates());
        });
    } else {
        placemark.geometry.setCoordinates(coords);
    }
};

/**
 * Get address by coordinates
 * @param  {array} coords Coordinates of the placemark
 * @return {void}
 */
function getAddress(coords)
{
    ymaps.geocode(coords, {kind: 'house', results: 1}).then(function (res) {
        var address = res.geoObjects.get(0).properties.get('text');
        $('#address').val($.trim(address.replace("Россия, ","")));
    });
}

/**
 * Initialize yandex map
 * @return {void}
 */
function init ()
{
    map = new ymaps.Map('map', {
        center: [50.28, 127.55],
        zoom: 12,
        controls: ['zoomControl']
    }, {
        searchControlProvider: 'yandex#search'
    });

    map.events.add('click', function (e) {
        var coords = e.get('coords');
        createPlacemark(coords);
        getAddress(coords);
    });

    bounds = map.getBounds();

	getCoordsByAddress();
}

/**
 * Search coordinates by address
 * @return {void}
 */
function getCoordsByAddress()
{
	var geocoder = ymaps.geocode($('#address').val(), {strictBounds: true, boundedBy: bounds});
    geocoder.then(
        function (response) {
            var coords = response.geoObjects.get(0).geometry.getCoordinates();
            createPlacemark(coords);
            map.panTo(coords, {flying: 1});
        }
    );
}

/**
 * Address change event
 * @param  {object} event Event
 * @return {void}
 */
$(document).on('change', '#address', function(event) {
	getCoordsByAddress();
});