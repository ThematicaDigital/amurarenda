/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
"use strict";

require('./bootstrap');
require('slick-carousel');
require('ekko-lightbox');

window.Vue = require('vue');

import Vue from 'vue'
import VueRouter from 'vue-router';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.use(VueRouter);

const AdvertSearch = require('./components/advert-search/AdvertsSearch.vue');

const AdvertListIndex = require('./components/advert-search/AdvertsListIndex.vue');
const AdvertsMap = require('./components/advert-search/AdvertsMap.vue');

const routes = [
	{path: '/', component: AdvertListIndex},
	{path: '/map', component: AdvertsMap}
];

const router = new VueRouter({
	routes,
});

Vue.component('advert-search', AdvertSearch);
Vue.component('notifications', require('./components/Notifications.vue'));
Vue.component('yandex-map', require('./components/YandexMap.vue'));
Vue.component('show-phone', require('./components/ShowPhone.vue'));
Vue.component('image-upload', require('./components/Dropzone.vue'));
Vue.component('comments', require('./components/Comments.vue'));

const app = new Vue({
	router
}).$mount('#app');

$(document).on('click', '#additional-parameters-string', () => {
	$('#additional-parameters-fields').fadeToggle();
});

$(document).ready(function () {
	const slickCarousel = $('#carousel');
	slickCarousel.removeClass('carousel-script-not-loaded');
	slickCarousel.slick({
		"arrows": true,
		"dots": false,
		"autoplay": true,
		"adaptiveHeight": true,
		"autoplaySpeed": 4000,
		"slidesToShow": 3,
		"swipe": false,
		"slidesToScroll": 1,
		nextArrow: '<span class="fa-stack fa-3x carousel-next-arrow fa-inverse"><i class="fa fa-circle-thin fa-stack-2x"></i><i class="fa fa-chevron-right fa-stack-1x"></i></span>',
		prevArrow: '<span class="fa-stack fa-3x carousel-prev-arrow fa-inverse"><i class="fa fa-circle-thin fa-stack-2x"></i><i class="fa fa-chevron-left fa-stack-1x"></i></span>',
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
					arrows: false,
					swipe: true
				}
			}
		]
	});
	$(document).on('click', '[data-toggle="lightbox"]', function (event) {
		event.preventDefault();
		$(this).ekkoLightbox({
			alwaysShowClose: true,
			showArrows: true
		});
	});
});
