export function getActiveIds(filters) {
	return filters.filter((item) => {
		return item.active === true;
	}).map((item) => {
		return item.id;
	});
}

export function assignActive(array, defaultActive, vueInstance) {
	array.forEach((item) => {
		vueInstance.$set(item, 'active', defaultActive);
	});

	return array;
}

export function getParameterByName(name) {
	const results = new RegExp('[?&]' + name.replace(/[\[\]]/g, '\\$&') + '(=([^&#]*)|&|#|$)')
		.exec(window.location.href);

	if (!results) {
		return null;
	}

	if (!results[2]) {
		return '';
	}

	return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

export function isNullOrUndefined(value) {
	return value === null || value === undefined;
}

export function isEmpty(value) {
	return isNullOrUndefined(value) || value === '' || isNaN(value);
}
