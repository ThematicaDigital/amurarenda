@extends('layouts/general')

@push('scripts_before')
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
@endpush

@section('content')

	<advert-search
			:preloaded_filters="{{$filters}}"
			:preloaded_adverts="{{$adverts}}">
	</advert-search>

@endsection
