@if(!$adverts->isEmpty())
	<div class="container-fluid main-advert-list">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h2>{{$header}}</h2>
				</div>
				@foreach($adverts as $advert)
					<div class="col-lg-3 col-md-4 col-sm-4 col-xs-6 padding-right-0 padding-bottom-15">
						@include("advert.advert-single")
					</div>
				@endforeach
				@if($isEnableAllAdvertsButton)
					<div class="btn-col col-sm-12">
						<a class="btn" href="{{ route('advert.list') }}">Все предложения</a>
					</div>
				@endif
			</div>
		</div>
	</div>
@endif
