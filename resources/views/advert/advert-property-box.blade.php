<div class="row advert-property-box">
	<div class=" phone">
		<show-phone id="{{ $advert->id }}"></show-phone>
	</div>
	<div class="map">
		<yandex-map coords="{{ $advert->yandex_coord }}"></yandex-map>
	</div>
</div>
<div class="row">
	@if (!$advert->options->isEmpty())
		<div class="col-xs-6 col-sm-12">
			<h5>
				Удобства
			</h5>
			<ul class="options-list">
				@foreach ($advert->options as $option)
					@if(!empty($option->img))
						<li>
							@svg($option->imageWithoutExtension, ['class'=>'options-list-icon'])
							<span>{{ $option->name }}</span>
						</li>
					@endif
				@endforeach
			</ul>
		</div>
		<hr>
	@endif
	@if (!$advert->features->isEmpty())
		<div class="col-xs-6 col-sm-12">
			<h5>
				Особенности
			</h5>
			<ul class="feature-list">
				@foreach ($advert->features as $feature)
					@if(!empty($feature->img))
						<li>
							@svg($feature->imageWithoutExtension, ['class'=>'feature-list-icon'])
							<span>{{ $feature->name }}</span>
						</li>
					@endif
				@endforeach
			</ul>
		</div>
	@endif
</div>
