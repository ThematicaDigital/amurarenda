<div class="single-advert">
	<a href="{{ route('advert.show', ['slug' => $advert->slug]) }}">
		<div class="single-advert-inner">
			<span class="apartment-type-label {{$advert->objType->slug}}">{{$advert->objType->name}}</span>
			{!! Html::image(Image::url($advert->mainImage,700,500,['crop']), null, ['class' => 'img-responsive', 'alt' => $advert->title]) !!}
			<div class="single-advert-details-block">
				<div class="details">
					<div>
						<div class="feature-title">
							Комнат
						</div>
						<h4>
							{{$advert->formatted_room_num}}
						</h4>
					</div>
					<div>
						<div class="feature-title">
							Площадь, м<sup>2</sup>
						</div>
						<h4>{{$advert->square}}</h4>
					</div>
					<div>
						<div class="feature-title">
							руб. / мес.<sup>2</sup>
						</div>
						<h4>{{$advert->price}}</h4>
					</div>
				</div>
				<div class="title">
					{{$advert->limited_title}}
				</div>
				<div class="address">
					<i class="fa fa-map-marker fa-lg" aria-hidden="true"></i> <span>{{str_limit($advert->address, 15)}}</span>
				</div>
			</div>
		</div>
	</a>
</div>