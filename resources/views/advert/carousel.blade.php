<div id="carousel" class="carousel-script-not-loaded" data-slick='{
}'>
	@foreach ($advert->images as $slide)
		<div class="slide" href="{{url(Image::url($slide, 1920, 1080, ['crop']))}}" data-toggle="lightbox"
		     data-title="{{ ucfirst($advert->title) }}" data-gallery="example-gallery">
			{!! Html::image(Image::url($slide, 800, 600, ['crop']), null, ['class' => 'img-responsive']) !!}
		</div>
	@endforeach
</div>

