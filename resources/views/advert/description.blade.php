<div class="show-advert container">
	<div class="row">
		<div class="advert-head-properties col-sm-4">
			<div class="row">
				<div class="col-xs-4">
					<div class="value">{{$advert->formatted_room_num}}</div>
					<div class="key">Комнат</div>
				</div>
				<div class="centered col-xs-4">
					<div class="value">{{$advert->square}}</div>
					<div class="key">Площадь м <sup>2</sup></div>
				</div>
				<div class="col-xs-4">
					<div class="value">{{$advert->price}}</div>
					<div class="key">руб./ месяц</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class=" col-sm-4">
			@include('advert.advert-property-box')
		</div>
		<div class=" col-sm-8">
			@include('advert.advert-body')
		</div>
	</div>
	@include('advert.advert-list', ['header' => "Другие $advert->room_num комнатные квартиры", 'adverts' => $anotherAdvertsByRoomNum, 'isEnableAllAdvertsButton' => false])

</div>
