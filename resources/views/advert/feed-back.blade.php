<div class="feedback">
	<comments advert_id="{{$advert->id}}"></comments>
	<div class="feedback-form">
		{{ Form::open(array('method' => 'POST','url' => route('comment.send'))) }}
		<div class="form-group">
			{{ Form::text('name', '', [ 'required', 'placeholder' => 'Ваше имя', 'class' => 'form-control']) }}
		</div>
		<div class="form-group">
			{{ Form::textarea('text', '', [ 'required', 'placeholder' => 'Ваш отзыв', 'rows' => 3, 'class' => 'form-control']) }}
		</div>
		<div class="form-group">
			{{ Form::submit('Оставить отзыв', [ 'class' => "btn btn-default"]) }}
		</div>
		{{ Form::hidden('advert_id', $advert->id) }}
		{{ Form::close() }}
	</div>
</div>