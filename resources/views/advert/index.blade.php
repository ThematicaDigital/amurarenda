@extends('layouts/general')

@push('scripts_before')
<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
@endpush

@section('content')
	@include('advert/carousel', ['advert' => $advert ])
	@include('advert/description', ['anotherAdvertsByRoomNum' => $anotherAdvertsByRoomNum])

@endsection
