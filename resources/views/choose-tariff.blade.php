@extends('layouts/general')

@section('content')
    <div class="choose-tariff">
        <div class="container-fluid-custom  header-margin padding-bottom-40">
            <div class="row">
                <div class="col-xs-12">
                    <h3>Полный доступ к базе</h3>
                    {!! Breadcrumbs::render('profile-tariff') !!}
                </div>
            </div>
            <div class="row">
            <div class="col-md-3 col-sm-4 left-column">
                <div> Выберите <br>
                    <strong> Тарифный план </strong>
                </div>
            </div>
            <div class="col-md-9 col-sm-8 row-tariff">
                @foreach($tariffs as $tariff)
                    <div class="col-sm-4 col-xs-12 panel-tariff">
                        <div class="tariff-description">
                            <div class="head-container">
                                @if ($tariff->days)
                                    <div class="name">
                                        <span>{{ $tariff->days }}</span>
                                        <span class="written">Дней</span>
                                    </div>
                                @else
                                    <div class="name">
                                        <span>30%</span>
                                        <span class="written">Премиум</span>
                                    </div>
                                @endif
                                <div class="body">
                                    {!! $tariff->description !!}
                                </div>
                                @if ($tariff->price)
                                    <div class="cost">{{ $tariff->price }} <span> руб </span></div>
                                @endif
                                @if ($tariff->price_old)
                                    <div class="cost-undercase"> {{ $tariff->price_old }} рублей</div>
                                @endif
                            </div>
                            @if ($tariff->days)
                                <a href="{{ route('robokassa.payment', $tariff->id) }}" class="btn btn-info">Выбрать</a>
                            @else
                                <a href="#requestModal" class="btn btn-info" data-toggle="modal">Выбрать</a>
                            @endif
                        </div>
                    </div>
                @endforeach
                @if ($page)
                    <div class="col-sm-12 tariff-general-description">
                        {!! $page->text !!}
                    </div>
                @endif
            </div>
            </div>
        </div>
    </div>
    @include("include.request-modal")
@endsection