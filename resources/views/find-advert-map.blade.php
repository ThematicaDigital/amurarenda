@extends('layouts/general')


@section('content')
    {!! Breadcrumbs::render('search-map') !!}

    <script type="text/javascript">
        ymaps.ready(init);
        var myMap,
            myPlacemark;

        function init() {
            myMap = new ymaps.Map("map", {
                center: [55.76, 37.64],
                zoom: 7
            });

            myPlacemark = new ymaps.Placemark([55.76, 37.64], {
                hintContent: 'Москва!',
                balloonContent: 'Столица России'
            });

            myMap.geoObjects.add(myPlacemark);
        }
    </script>




    <div class="container-fluid" style="height:100%;margin:0px;padding:0px">
        <div id="map" class="row" style="width:110%; height:100%;">
        </div>
    </div>


@endsection
