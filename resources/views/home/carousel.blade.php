<div class="container-fluid header-margin main-section-1">
	<div class="row">
		<div class="col-md-12">
			<div class="row hidden-xs">
				<div class="carousel slide" data-ride="carousel">
					<div class="carousel-inner" role="listbox">
						@if($banners)
							@foreach ($banners as $banner)
								<div class="item {{ ($loop->first) ? 'active' : '' }}">
									<a href= {{$banner->link}}>
										{!! Html::image(Image::url($banner->img, 1920, 540, array('crop')), $banner->title, ['class' => 'img-responsive']) !!}
										<div class="container hidden-xs hidden-sm text-panel">
											<div class="row">
												<div class="col-md-5">
													<h1>
														{{$banner->title}}
													</h1>
												</div>
											</div>
										</div>
									</a>
								</div>
							@endforeach
						@endif
					</div>
				</div>
			</div>
			@include('home/search-filter')
		</div>
	</div>
</div>
