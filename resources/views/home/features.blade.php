<div class="container-fluid features">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-12">
                <h2>Наши преимущества</h2>
            </div>

            <div class="col-md-3 col-sm-6 col-xs-6">
                {!! Html::image('images/transfer-1.svg', null, ['class' => 'img-responsive']) !!}
                {!! App\Http\Controllers\PageController::preview('without-intermediaries') !!}
            </div>

            <div class="col-md-3 col-sm-6 col-xs-6">
                {!! Html::image('images/piggy-bank.svg', null, ['class' => 'img-responsive']) !!}
                {!! App\Http\Controllers\PageController::preview('without-commission') !!}
            </div>

            <div class="col-md-3 col-sm-6 col-xs-6">
                {!! Html::image('images/office-block.svg', null, ['class' => 'img-responsive']) !!}
                {!! App\Http\Controllers\PageController::preview('large-base') !!}
            </div>

            <div class="col-md-3 col-sm-6 col-xs-6">
                {!! Html::image('images/refresh.svg', null, ['class' => 'img-responsive']) !!}
                {!! App\Http\Controllers\PageController::preview('daily-update') !!}
            </div>
        </div>
    </div>
</div>
