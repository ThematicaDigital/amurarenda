@extends('../layouts/general')


@section('content')

	@include('home/carousel')
	@include('home/features')
	@include('advert/advert-list', ['header' => 'Последние предложения', 'isEnableAllAdvertsButton' => true])

@endsection