<div class="container glass-panel">
	<div class="row">
		{!! Form::open(['action' => 'AdvertController@index', 'method' => 'get']) !!}
		<div class="col-lg-3 col-md-2 col-sm-6 select-wrapper">
			{!! Form::select('rent-types', $rentTypes, null, ['class' => 'selector']) !!}
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6">
			@foreach($apartmentTypes as $apartmentType)
				<div class="col-sm-4">
					{!! Form::radio('apartment-types', $apartmentType->id, null, ['class' => 'hidden-control', 'id' => $apartmentType->slug]) !!}
					{!! Form::label($apartmentType->slug, $apartmentType->name, ['class' => 'apartment-types-radio ' . $apartmentType->slug]) !!}
				</div>
			@endforeach
		</div>
		<div class="col-lg-5 col-md-6 col-sm-12 col-xs-12 remove-left-padding-sm">
			<div class="col-sm-5 col-xs-12">
				<div class="row-wrapper radio-container ">
					{!! Form::radio('rooms-value', '1', null, ['class' => 'hidden-control', 'id' => 'one-room']) !!}
					{!! Form::label('one-room', '1', ['class' => 'rooms-value']) !!}
					{!! Form::radio('rooms-value', '2', null, ['class' => 'hidden-control', 'id' => 'two-rooms']) !!}
					{!! Form::label('two-rooms', '2', ['class' => 'rooms-value']) !!}
					{!! Form::radio('rooms-value', '3', null, ['class' => 'hidden-control', 'id' => 'three-rooms']) !!}
					{!! Form::label('three-rooms', '3', ['class' => 'rooms-value']) !!}
					{!! Form::radio('rooms-value', '4', null, ['class' => 'hidden-control', 'id' => 'more-rooms']) !!}
					{!! Form::label('more-rooms', '4+', ['class' => 'rooms-value']) !!}
				</div>
			</div>
			<div class="col-sm-7 col-xs-12">
				{!! Form::button('<i class="fa fa-search" aria-hidden="true"></i>Найти объявление', ['type' => 'submit', 'class' => 'button btn-danger']) !!}
			</div>
		</div>
		{!! Form::close() !!}
	</div>
</div>
