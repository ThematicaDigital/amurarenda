<div class="container">
    <div class="modal fade" id="authModal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="row modal-header">

                    <div id="logintitle" class="col-sm-4 col-xs-10">
                        <h3>Вход</h3>
                    </div>
                    <div class="col-sm-1 col-xs-2 visible-xs">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div id="logindescription" class="col-sm-7 col-xs-12 margin-top-30">
                        Ещё не зарегистрированы? Тогда <a href="#registerModal" class="main-red" data-toggle="modal" data-dismiss="modal">Регистрируйтесь!</a>
                    </div>
                </div>


                <div id="loginform">
                    {{ Form::open(array('method' => 'POST','url' => url('login'))) }}
                        <div class="row modal-body">
                            <div class="col-md-12">

                                {{ Form::email('email', '', [ 'required' => '', 'placeholder' => 'Ваша почта', ]) }}
                            </div>
                            <div class="col-md-12">
                                {{ Form::password('password', [ 'required' => '', 'placeholder' => 'Пароль', ]) }}
                            </div>
                        </div>
                        <div class="row modal-footer">
                            <div class="col-md-6">
                                {{ Form::submit('Войти', [ 'class' => "btn btn-danger"]) }}
                            </div>
                            <div class="col-md-6">
                                <span class="acceptlicence-line">Забыли свой пароль?</span><br>
                                <a href="#forgotModal" class="main-red" data-toggle="modal" data-dismiss="modal">Восстановить</a>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>

            </div>
        </div>
    </div>
</div>
