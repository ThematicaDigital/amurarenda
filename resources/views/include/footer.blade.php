<div class="footer">
	<div class="footer-top">
		<div class="container-fluid-custom">
			<div class="row">
				<div class="col-md-6 col-xs-12 wrapper">
					<div class="col-xs-6">
						<h4>Амураренда</h4>
						<a href="{{ route('page.show', ['polzovatelskoe-soglashenie']) }}"> Пользовательское соглашение </a> <br>
						<a href="{{ route('page.show', ['oferta']) }}"> Оферта </a> <br>
					</div>
					<div class="col-xs-6">
						<h4>Арендаторам</h4>
						<a href="{{ route('advert.list') }}"> Найти квартиру </a> <br>
						<a href="/advert/list#/map"> Найти квартиру по карте </a> <br>
						<a href="{{ route('pricing') }}"> Полный доступ </a> <br>
					</div>
				</div>
				<div class="col-md-6 col-xs-12 wrapper" >
					<div class="col-xs-6">
						<h4>Арендодателям</h4>
						<a href="{{ route('advert.add', ['obj_type_id=3']) }}"> Сдать квартиру </a> <br>
						<a href="{{ route('advert.add', ['obj_type_id=2']) }}"> Сдать комнату </a> <br>
						<a href="{{ route('advert.add') }}"> Сдать коттедж </a> <br>
					</div>
					<div class="col-xs-6">
						<h4>Наши контакты</h4>
						<a href="tel:+79619527090"> +7(961)952-70-90 </a> <br>
						<a href="tel:+79619537548"> +7(961)953-75-48 </a> <br>
						<p>Благовещенск, Ленина, 115, 2 этаж, 20 кабинет</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container-fluid-custom">
			<div class="row">
				<div class="col-md-3  col-xs-12 link-back">
					<a href="http://info.amurarenda.ru"><u>info.amurarenda.ru</u></a>
				</div>
				<div class="col-md-9  col-xs-12 copyright-text">
					Использование материалов сайта<a class="main-red" href="/">&nbsp; amurarenda.ru &nbsp;</a>
					возможно только при наличии активной ссылки на первоисточник.
				</div>
			</div>
		</div>
	</div>
</div>