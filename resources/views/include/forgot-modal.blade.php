<div class="container">
    <div class="modal fade" id="forgotModal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="row modal-header">
                    <div id="logintitle" class="col-sm-10 col-xs-10">
                        <h3>Восстановить пароль</h3>
                    </div>
                    <div class="col-sm-1 col-xs-2 visible-xs">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                </div>
                <div id="loginform">
                    {{ Form::open(array('method' => 'POST','url' => url('password/email'))) }}
                        <div class="row modal-body">
                            <div class="col-md-12">
                                {{ Form::email('email', '', [ 'required' => '', 'placeholder' => 'Ваша почта', ]) }}
                            </div>
                        </div>
                        <div class="row modal-footer">
                            <div class="col-md-6">
                                {{ Form::submit('Восстановить', [ 'class' => "btn btn-danger"]) }}
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
