<div class="header">
	<div class="header-top hidden-xs">
		<div class="container-fluid-custom flex-row-space-between-container">
			<div class="">
				<i class="fa fa-mobile" aria-hidden="true"></i>
				+7(914)952-70-90
				<i class="fa fa-circle icon-dot" aria-hidden="true"></i>
				+7(961)953-75-48
			</div>
			<div class="hidden-sm hidden-md">
				<i class="fa fa-map-marker" aria-hidden="true"></i>
				Благовещенск, Ленина, 115, 2 этаж, 20 кабинет
			</div>
			<div class="enter-label">
				@include('user.user-header-menu')
			</div>
		</div>
	</div>

	<div class="header-bottom">
		<div class="container-fluid-custom toggle-header-bottom-view">
			<div class="logo-box">
				<a class="" href="/">
					<img class="img-responsive" src={{ URL::asset('images/logo.png')}}>
				</a>
				{{--<a class="hamburger hidden-sm hidden-md hidden-lg" href="/">--}}
					{{--<i class="fa fa-bars" aria-hidden="true"></i>--}}
				{{--</a>--}}
			</div>
			<a class="nav-link hidden-xs" href="{{ route('advert.list') }}#/">
				<i class="fa fa-search fa-2x" aria-hidden="true"></i>
				<span>Найти квартиру</span>
			</a>
			<a class="nav-link hidden-xs" href="{{ route('advert.list') }}#/map">
				<i class="fa fa-map-marker fa-2x" aria-hidden="true"></i>
				<span>Квартиры на карте</span>
			</a>
			<div class="btn-box">
				<a href="{{ route('pricing') }}" class="btn btn-info btn-round">
					Полный доступ
				</a>
				<a href="{{ route('advert.add') }}" class="btn btn-danger btn-round">
					Сдать квартиру
				</a>
			</div>
		</div>
	</div>

</div>
