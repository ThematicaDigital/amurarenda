@if($adverts)
    @foreach($adverts as $advert)
        <div class="col-lg-3 col-sm-4 col-xs-12 padding-right-0 padding-bottom-15">
            @include("advert.advert-single")
        </div>
    @endforeach
@endif
