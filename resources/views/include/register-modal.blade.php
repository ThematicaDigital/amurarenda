<div class="container">
    <div class="modal fade" id="registerModal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="row modal-header">

                    <div id="registertitle" class="col-sm-6 col-xs-10">
                        <h3>Регистрация</h3>
                    </div>
                    <div class="col-sm-1 col-xs-2 visible-xs">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div id="registerdescription" class="col-sm-5 col-xs-12 margin-top-30">
                        Вы тут уже были? Тогда <a href="#authModal" class="main-red" data-toggle="modal" data-dismiss="modal">Войдите</a>
                    </div>
                </div>

                <div id="registerform">
                    {{ Form::open(array('method' => 'POST','url' => url('register'))) }}
                        <div class="row modal-body">
                            <div class="col-md-12">
                                {{ Form::email('email', '', [ 'required' => '', 'placeholder' => 'Ваша почта', ]) }}
                            </div>
                            <div class="col-md-12">
                                {{ Form::password('password', [ 'required' => '', 'placeholder' => 'Пароль' ]) }}
                            </div>
                            <div class="col-md-12">
                                {{ Form::password('password_confirmation', [ 'required' => '', 'placeholder' => 'Повторите пароль' ]) }}
                            </div>
                        </div>
                        <div class="row modal-footer">
                            <div class="col-md-6">
                                {{ Form::submit('Зарегистрироваться', [ 'class' => "btn btn-danger"]) }}
                            </div>
                            <div class="col-md-6">
                                <span class="acceptlicence-line">Регистрируясь, вы принимаете условия</span><br>
                                <a href="{{ route('page.show', ['polzovatelskoe-soglashenie']) }}" target="_blank" class="main-red user-accept">пользовательского соглашения</a>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
