<div class="container">
    <div class="modal fade" id="requestModal" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="row modal-header">
                    <div id="logintitle" class="col-sm-4 col-xs-10">
                        <h3>Премиум 30%</h3>
                    </div>
                    <div class="col-sm-1 col-xs-2 visible-xs">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                </div>
                <div id="requestform">
                    {{ Form::open(array('method' => 'POST','url' => route('request'))) }}
                        <div class="row modal-body">
                            <div class="col-md-12">
                                {{ Form::text('name', '', [ 'required' => '', 'placeholder' => 'Ваше имя']) }}
                            </div>
                            <div class="col-md-12">
                                {{ Form::text('phone', '', [ 'required' => '', 'placeholder' => 'Ваш номер телефона']) }}
                            </div>
                            <div class="col-md-12">
                                {{ Form::email('email', '', [ 'placeholder' => 'Ваша почта']) }}
                            </div>
                            <div class="col-md-12">
                                {{ Form::textarea('text', '', [ 'placeholder' => 'Ваше сообщение', 'rows' => 3]) }}
                            </div>
                        </div>
                        <div class="row modal-footer">
                            <div class="col-md-12">
                                {{ Form::submit('Выбрать', [ 'class' => "btn btn-danger"]) }}
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>

            </div>
        </div>
    </div>
</div>
