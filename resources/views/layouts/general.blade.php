<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> Амураренда - @yield('title') </title>

    <link rel="shourtcut icon" type="image/x-icon" href="{{ asset('/images/favicon.ico') }}">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=PT+Serif" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.1.1/ekko-lightbox.min.css"/>
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    @stack('styles')
</head>
<body>

<div id="app">

    @include('include.notifications')
    @include('include.header')

    <div id="content">
        @yield('content')
    </div>

    @include('include.footer')
</div>

@if (Auth::guest())
    @include("include.register-modal")
    @include("include.auth-modal")
    @include("include.forgot-modal")
@endif

@stack('scripts_before')
<script src="{{ asset('/js/app.js') }}"></script>
@stack('scripts_after')
</body>
</html>
