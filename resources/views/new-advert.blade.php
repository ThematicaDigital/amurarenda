@extends('layouts/general')

@push('scripts_after')
	<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU"></script>
	<script src="{{ asset('packages/sleepingowl/custom/adverts_map.js') }}"></script>
@endpush

@section('content')
    <div class="new-advert header-margin">
	    <div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h3>Сдать квартиру</h3>
					{!! Breadcrumbs::render('advert-create') !!}
				</div>
			</div>

		    @if(isset($advert))
		    	{{ Form::model($advert, ['route' => ['advert.update', $advert->id], 'id' => 'new-advert-form']) }}
		    @else
		    	{{ Form::open(['url' => route('advert.store'), 'id' => 'new-advert-form']) }}
		    @endif

		    {{-- APARTMENT TYPE SELECTION --}}
			<div class="col-md-4 col-sm-3 margin-top-30px padding-left-0 padding-right-0">
				Шаг 1 - Выберите что вы сдаёте
			</div>
			<div class="col-md-8 col-sm-9 object-switch-buttons margin-top-30px">
				@foreach($apartmentTypes as $apartmentType)
					<div class="col-sm-4 ">
						{!! Form::radio('obj_type_id', $apartmentType->id, (old('obj_type_id')) ? old('obj_type_id') : $loop->first, [' class'=> 'hidden-control', ' id'=>"aptType-{ $apartmentType->slug }"]) !!}
						{!! Form::label ("aptType-{ $apartmentType->slug }", $apartmentType->name , ['class' => "apartment-btn {$apartmentType->slug}"]) !!}
					</div>
				@endforeach
			</div>

		    {{-- ADDRESS SELECTION --}}
			<div class="row">
				<div class="col-md-4 col-sm-3 margin-top-30px">
					Шаг 2 - Введите адрес *
				</div>
				<div class="col-md-8 col-sm-9 margin-top-30px address-panel">
					{!! Form::text('address', old('address'), ['id' => 'address', 'placeholder' => 'Например: 50 лет Октября, 204']) !!}
					<div id="map" style="width:100%; height:350px;"></div>
					<div class="description">
						Убедитесь, что адрес отображается на карте и введён правильно.<br>
						Уточните, при необходимости, местонахождение объекта аренды кликом по карте.
					</div>
				</div>
			</div>
		    {{-- DISTRICT SELECTION --}}
			<div class="row">
				<div class="col-md-4 col-sm-3 margin-top-30px">
					Выберите район города *
				</div>
				<div class="form-group col-md-8 col-sm-9 margin-top-30px">
					{!! Form::select('district_id', $districts, (old('district_id')) ? old('district_id') : null, ['class' => 'form-control']) !!}
				</div>
			</div>
		    {{-- RENT TYPE SELECTION --}}
			<div class="row">
				<div class="col-md-4 col-sm-3 margin-top-30px">
					Шаг 3 - Выберите тип аренды *
				</div>
				<div class="form-group col-md-8 col-sm-9 margin-top-30px btn-group">
					{!! Form::select('rent_type_id', $rentTypes, (old('rent_type_id')) ? old('rent_type_id') : null, ['class' => 'form-control']) !!}
				</div>
			</div>

		    {{-- TITLE INPUT --}}
			<div class="row">
				<div class="col-md-4 col-sm-3 margin-top-30px">
					Заголовок *
				</div>
				<div class="form-group col-md-8 col-sm-9 margin-top-30px">
					{!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => 'Например &ldquo; Двухкомнатная квартира на ленина &rdquo;']) !!}
				</div>
			</div>
		    {{-- DESCRIPTION INPUT --}}
			<div class="row">
				<div class="col-md-4 col-sm-3 margin-top-30px">
					Описание *
				</div>
				<div class="form-group col-md-8 col-sm-9 margin-top-30px">
					{!! Form::textarea('text', old('text'), ['class' => 'form-control', 'placeholder' => 'Например 	&ldquo; Расположение от центра близость школ и т.д. &rdquo;']) !!}
				</div>
			</div>

		    {{-- PHOTO DOWNLOAD CONTROLS --}}
			<div class="row">
				<div class="col-md-4 col-sm-3 margin-top-30px">
					Фотографии
				</div>
				<div class="form-group col-sm-9 col-md-8 btn-group margin-top-30px">
					<image-upload url="{{ route('advert.store.image-upload') }}" images="{{ (isset($advert) ? json_encode($advert->images) : '') }}" base-path="{{ config('app.url') }}">
						{{ csrf_field() }}
					</image-upload>
					<div id="file-inputs"></div>
				</div>
			</div>
		    {{-- ROOM COUNT INPUT --}}
			<div class="row">
				<div class="col-md-4 col-sm-3 margin-top-30px">
					Количество комнат *
				</div>
				<div class="form-group room-count col-sm-12 col-xs-12 col-md-8 btn-group margin-top-30px">
					@foreach($roomsCount as $room)
						{!! Form::radio('room_num', $room,  (old('room_num')) ? old('room_num') : $loop->first, ['id' => "rooms{$room}", 'class' => 'hidden-control']) !!}
						{!! Form::label("rooms{$room}", $room, ['class' => 'col-xs-1 col-sm-1 text-center']) !!}
					@endforeach
				</div>
			</div>

		    {{-- PARAMS INPUT --}}
		    <div class="row">
				<div class="col-md-4 col-sm-3 margin-top-30px">
					Параметры *
				</div>
				<div class="col-md-8 col-sm-9 margin-top-30px">
					<div class="row">
						<div class="form-group col-xs-12 col-sm-6 col-md-3">
							<div>{!! Form::label('square', 'Площадь') !!}</div>
							<div class="input-group">
								{!! Form::number('square', old('square'), ['class' => 'form-control', 'min' => 1, 'max' => 1000, 'id' => 'square', 'aria-describedby' => "square-addon"]) !!}
								<span class="input-group-addon" id="square-addon">м <sup>2</sup></span>
							</div>
						</div>
						<div class="form-group col-xs-12 col-sm-6 col-md-2">
							<div>{!! Form::label('floor', 'Этаж') !!}</div>
							{!! Form::number('floor', old('floor'), ['class' => 'form-control', 'min' => 1, 'max' => 100, 'id' => 'floor']) !!}
						</div>
					</div>
				</div>
			</div>

		    {{-- PRICE INPUT --}}
			<div class="row">
				<div class="col-md-4 col-sm-3 margin-top-30px">
					Цена в месяц *
				</div>
				<div class="form-group col-md-8 col-sm-9 margin-top-30px">
					<div class="input-group col-xs-12 col-sm-6 col-md-3">
						{!! Form::number('price', old('price'), ['class' => 'form-control', 'aria-describedby' => "price-addon", 'min' => 0]) !!}
						<span class="input-group-addon" id="price-addon">руб.</span>
					</div>
				</div>
			</div>

		    {{-- ADDITIONAL INPUTS --}}
			<div class="row">
				<div class="col-md-4 col-sm-3 margin-top-30px"></div>
				<div class="col-md-8 col-sm-9 options margin-top-30px">
					<div class="col-sm-12 margin-bottom-30px padding-left-0 padding-right-0">
						<span id="additional-parameters-string">Дополнительные параметры (заполнять по желанию)</span>
					</div>
				</div>
			</div>
		    <div id="additional-parameters-fields">
			    {{-- OPTIONS SELECTION --}}

				<div class="row">
					<div class="col-md-4 col-sm-3 margin-top-30px">
						{!! Form::label('options', 'Удобства') !!}
					</div>
					<div class="form-group options-panel col-md-8 col-sm-9 features margin-top-30px">
						@foreach ($options as $option)
							@if(!empty($option->img))
								<div class="col-xs-6 col-sm-4 col-md-3 padding-left-0 padding-right-0">
									{!! Form::checkbox("options[{$option->id}]", $option->name, (isset($advert) && (in_array($option->id, $advert->options->pluck('id')->all()))) ? true : false, ['id' => "option-{$option->id}", 'class' => 'hidden-control']) !!}
									<label for="option-{{$option->id}}">
										<div class="text-right">
											@svg("images/success")
										</div>
										@svg($option->imageWithoutExtension, ['class' => 'option-icon'])
										{{ $option->name }}
									</label>
								</div>
							@endif
						@endforeach
					</div>
				</div>

			    {{-- FEATURES SELECTION --}}
				<div class="row">
					<div class="col-md-4 col-sm-3 margin-top-30px">
						{!! Form::label('features', 'Особенности') !!}
					</div>
					<div class="form-group features-panel col-md-8 col-sm-9 features padding-left-0 padding-right-0 margin-top-30px">
						<div class="row">
							@foreach ($features as $feature)
								<div class="col-xs-12 col-sm-6 col-md-4">
									{!! Form::checkbox("features[{$feature->id}]", $feature->name, (isset($advert) && (in_array($feature->id, $advert->features->pluck('id')->all()))) ? true : false, ['id' => "feature-{$feature->id}"]) !!}
									{!! Form::label("feature-{$feature->id}", $feature->name) !!}
								</div>
							@endforeach
						</div>
					</div>
				</div>
		    </div>

		    {{-- PHONE INPUT --}}

			<div class="row">
				<div class="col-md-4 col-sm-3 margin-top-30px">
					{!! Form::label('phone', 'Телефон') !!}
				</div>
				<div class="form-group col-md-8 col-sm-9 margin-top-30px">
					<div class="col-xs-12 col-sm-6 col-md-3 padding-left-0 padding-right-0">
						{!! Form::text('phone_num', old('phone_num'), ['class' => 'form-control', 'id' => 'phone']) !!}
					</div>
				</div>
			</div>
		    {{-- HIDDEN INPUTS --}}
		    {!! Form::hidden('yandex_coord', old('yandex_coord'), ['class' => '', 'id' => 'yandex_coord']) !!}
		    {!! Form::hidden('id') !!}

		    {{-- SUBMIT BUTTON --}}
		    <div class="col-md-3 col-sm-12 padding-left-0 padding-right-0 margin-top-30px">
			    {!! Form::submit((isset($advert)) ? 'Изменить' : 'Отправить на модерацию', ['id' => 'createnewadv', 'class' => 'btn btn-danger send-button margin-top-30px']) !!}
		    </div>
		    {!! Form::close() !!}
	    </div>
	</div>
@endsection
