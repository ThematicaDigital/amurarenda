@if ($page)
	<h5>{{ $page->title }}</h5>
	<div class="article">
		{!! $page->text !!}
	</div>
@endif