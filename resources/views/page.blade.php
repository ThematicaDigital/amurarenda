@extends('layouts/general')

@section('content')
    <div class="header-margin">
	    <div class="container">
		    {!! Breadcrumbs::render('page-show', $page) !!}
		    <h2>{{ $page->title }}</h2>
		    {{ $page->text }}
	    </div>
	</div>
@endsection
