@extends('layouts/general')

@section('content')
	<div class="notifications">
		<div class="container-fluid-custom header-margin padding-bottom-40">
			<div class="row">
				{!! Breadcrumbs::render('profile-notifications') !!}
				<div class="col-md-3 col-sm-12 margin-top-30">
					@include('user.nav-bar')
				</div>
				<div class="col-md-9 col-sm-12 margin-top-30">
					@yield('section')
				</div>
			</div>
		</div>
	</div>
@endsection
