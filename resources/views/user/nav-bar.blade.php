<div class="nav-bar">
    <a href="{{ route('user.adverts') }}" class="btn btn-default">
        Мои объявления
    </a>
    <a href="{{ route('user.payments') }}" class="btn btn-default">
        Мои подписки
    </a>
    <a href="{{ route('user.notifications') }}" class="btn btn-default">
        Уведомления
    </a>
</div>
