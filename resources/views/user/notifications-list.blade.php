<div class="notifications-list">
	@foreach ($notifications as $notification)
		<div class="row">
			<div class="col-xs-3">
				<time datetime="{{ $notification->created_at }}">{{ $notification->created_at }}</time>
			</div>
			<div class="col-xs-6">
				{{ $notification->data['message'] }}
			</div>
			<div class="col-xs-3 text-right">
				<a class="link" href="{{ $notification->data['action'] }}" target="_blank">перейти к объявлению</a>
			</div>


		</div>
	@endforeach
</div>