<div class="notifications-list">
	<div class="row">
		<div class="col-xs-3">
			<strong>Дата оплаты</strong>
		</div>
		<div class="col-xs-5">
			<strong>Описание</strong>
		</div>
		<div class="col-xs-3">
			<strong>Сумма</strong>
		</div>
		<div class="col-xs-1 text-right">
			<strong>Статус</strong>
		</div>
	</div>
	@foreach ($payments as $payment)
		<div class="row">
			<div class="col-xs-3">
				<time datetime="{{ $payment->created_at }}">{{ $payment->created_at }}</time>
			</div>
			<div class="col-xs-5">
				Оплата тарифного плана &laquo;{{ $payment->tariff->title }}&raquo;
			</div>
			<div class="col-xs-3">
				{{ $payment->sum }} руб.
			</div>
			<div class="col-xs-1 text-right">
				{{ $payment->status_title }}
			</div>
		</div>
	@endforeach
</div>