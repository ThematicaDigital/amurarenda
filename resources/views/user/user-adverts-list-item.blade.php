<div class="advert">
	<div class="img">
		{!! Html::image(Image::url($advert->mainImage,160,110,['crop']), null, ['class' => 'img-responsive', 'alt' => $advert->title]) !!}
		<span class="apartment-type-label {{$advert->objType->slug}}"> {{$advert->objtype->name}} </span>
	</div>
	<div class="description">
		<a href="{{ route('advert.show', [$advert->slug] )}}" class="title">
			{{ ucfirst( $advert->limited_title) }}
		</a>
		@if( $advert->publicated )
			<div class="status status-passed">
				{{ 'Опубликовано' }}
			</div>
		@endif
		@if($advert->declinetypes->count())
			<div class="status status-moderatorial">
				Причина отклонения: <br>
				@foreach($advert->declinetypes as $decline)
					{{$decline->name }} <br>
				@endforeach
			</div>
		@endif
		@if((!$advert->declinetypes->count()) && (!$advert->publicated))
			<div class="status status-waiting">
				ожидает прохождения модерации
			</div>
		@endif

	</div>
	<div class="text-right">
		<div class="created_at">
			{{ $advert->created_at }}
		</div>
	</div>

	<div class="advert-tool-box">
		<a href="{{ route('advert.edit', [$advert->id] )}}" class="btn btn-default">
			<i class="fa fa-pencil-square-o" aria-hidden="true"></i>
			<span>Редактировать</span>
		</a>

		<a href="{{ route('advert.delete', [$advert->id] )}}" class="btn btn-default">
			<i class="fa fa-times" aria-hidden="true"></i>
			<span>Удалить</span>
		</a>
	</div>
</div>
