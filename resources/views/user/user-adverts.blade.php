@extends('user.index')

@section('section')
	<div class="user-advert-list">
		@foreach( $adverts as $advert )
			@include('user.user-adverts-list-item')
		@endforeach
	</div>
@endsection

