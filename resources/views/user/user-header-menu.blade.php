@if (Auth::guest())
    <a href="#authModal" class="nav-link" data-toggle="modal">
        <i class="fa fa-user" aria-hidden="true"></i>
        Войти в кабинет
    </a>
@else
    <!-- <ul> -->
    <li class="dropdown">
        <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-expanded="false">
            <i class="fa fa-user" aria-hidden="true"></i>
            {{ Auth::user()->email }}
            <span class="caret"> </span>
        </a>
        <ul class="dropdown-menu" role="menu">
            @if(Auth::user()->isAdmin())
                <li>
                    <a class="nav-link" href="{{ route('admin.dashboard') }}">
                        <i class="fa fa-tachometer" aria-hidden="true"></i>
                        Админ панель
                    </a>
                </li>
            @endif
            <li>
                <a class="nav-link" href="{{ route('user.adverts') }}">
                    <i class="fa fa-file-image-o" aria-hidden="true"></i>
                    Мои объявления
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('user.payments') }}">
                    <i class="fa fa-star-o" aria-hidden="true"></i>
                    Мои подписки
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('user.notifications') }}">
                    <i class="fa fa-bell-o" aria-hidden="true"></i>
                    Уведомления
                </a>
            </li>
            <li>
                <a class="nav-link" href="{{ route('logout.get') }}">
                    <i class="fa fa-sign-out" aria-hidden="true"></i>
                    Выход
                </a>
            </li>
        </ul>
    </li>
    <!-- </ul> -->
@endif
