<?php
Breadcrumbs::register('home', function ($breadcrumbs) {
	$breadcrumbs->push('Главная', action('MainController@index'));
});

Breadcrumbs::register('advert-list', function ($breadcrumbs) {
	$breadcrumbs->parent('home');
	$breadcrumbs->push('Поиск', action('AdvertController@index'));
});

Breadcrumbs::register('advert-create', function ($breadcrumbs) {
	$breadcrumbs->parent('home');
	$breadcrumbs->push('Сдать квартиру', action('AdvertController@create'));
});

Breadcrumbs::register('advert-edit', function ($breadcrumbs) {
	$breadcrumbs->parent('home');
	$breadcrumbs->push('Редактировать объявление', action('AdvertController@edit'));
});

Breadcrumbs::register('profile-adverts', function ($breadcrumbs) {
	$breadcrumbs->parent('home');
	$breadcrumbs->push('Мои объявления', action('UserController@getAdverts'));
});

Breadcrumbs::register('profile-notifications', function ($breadcrumbs) {
	$breadcrumbs->parent('home');
	$breadcrumbs->push('Мои уведомления', action('UserController@notifications'));
});

Breadcrumbs::register('profile-tariff', function ($breadcrumbs) {
	$breadcrumbs->parent('home');
	$breadcrumbs->push('Тарифные планы', action('TariffController@choose'));
});

Breadcrumbs::register('page-show', function ($breadcrumbs, $page) {
	$breadcrumbs->parent('home');
	$breadcrumbs->push($page->title, action('PageController@show', $page->slug));
});