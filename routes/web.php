<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MainController@index')->name('home');

Route::get('/pricing', 'TariffController@choose')->name('pricing');
Route::post('/request', 'TariffController@request')->name('request');

Route::group(['prefix' => '/user'], function () {
	Route::get('adverts', 'UserController@getAdverts')->name('user.adverts');
	Route::get('payments', 'UserController@payments')->name('user.payments');
	Route::get('notifications', 'UserController@notifications')->name('user.notifications');
});

Route::group(['prefix' => 'advert'], function() {
	Route::get('add', 'AdvertController@create')->name('advert.add');
	Route::get('edit/{advert_id}', 'AdvertController@edit')->name('advert.edit');
	Route::get('delete/{advert_id}', 'AdvertController@delete')->name('advert.delete');
	Route::get('list', 'AdvertController@index')->name('advert.list');
	Route::get('{slug}', 'AdvertController@show')->name('advert.show');
	Route::post('store', 'AdvertController@store')->name('advert.store');
	Route::post('update/{advert_id}', 'AdvertController@update')->name('advert.update');

	Route::post('/upload/image', 'AdvertController@imageUpload')->name('advert.store.image-upload');
});

Route::get('/show-phone', 'AdvertController@showPhone');

/**
 * Comments
 */
Route::get('/get-comments', 'CommentController@getComments');
Route::post('/send-comment', 'CommentController@sendComment')->name('comment.send');

/**
 * Pages
 */
Route::group(['prefix' => '/page'], function () {
	Route::get('{slug}', 'PageController@show')->name('page.show');
});

/**
 * Pages
 */
Route::group(['prefix' => '/robokassa'], function () {
	Route::get('/result', 'RobokassaController@result')->name('robokassa.result');
	Route::get('/success', 'RobokassaController@success')->name('robokassa.success');
	Route::get('/failure', 'RobokassaController@failure')->name('robokassa.failure');
	Route::get('/payment/{tariff_id}', 'RobokassaController@payment')->name('robokassa.payment');
});

Route::get('/logout', 'Auth\LoginController@logout')->name('logout.get');
Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('/password/email', function (){
    abort(404);
});
Route::post('/register', 'Auth\RegisterController@register')->name('register.post');
Route::get('/register', function (){
    abort(404);
});
Route::post('/login', 'Auth\LoginController@login')->name('login.post');
Route::get('/login', function (){
    abort(404);
});